<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $dealerImage->name }}</p>
</div>

<!-- Url Field -->
<div class="col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    <p>{{ $dealerImage->url }}</p>
</div>

<!-- Dealer Id Field -->
<div class="col-sm-12">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    <p>{{ $dealerImage->dealer_id }}</p>
</div>

