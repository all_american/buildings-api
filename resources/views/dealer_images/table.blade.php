<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="dealer-images-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Url</th>
                <th>Dealer Id</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dealerImages as $dealerImage)
                <tr>
                    <td>{{ $dealerImage->name }}</td>
                    <td>{{ $dealerImage->url }}</td>
                    <td>{{ $dealerImage->dealer_id }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['dealerImages.destroy', $dealerImage->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('dealerImages.show', [$dealerImage->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('dealerImages.edit', [$dealerImage->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $dealerImages])
        </div>
    </div>
</div>
