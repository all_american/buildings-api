<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Dealer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    {!! Form::number('dealer_id', null, ['class' => 'form-control', 'required']) !!}
</div>