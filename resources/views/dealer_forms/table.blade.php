<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="dealer-forms-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Url</th>
                <th>Dealer Id</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dealerForms as $dealerForm)
                <tr>
                    <td>{{ $dealerForm->name }}</td>
                    <td>{{ $dealerForm->url }}</td>
                    <td>{{ $dealerForm->dealer_id }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['dealerForms.destroy', $dealerForm->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('dealerForms.show', [$dealerForm->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('dealerForms.edit', [$dealerForm->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $dealerForms])
        </div>
    </div>
</div>
