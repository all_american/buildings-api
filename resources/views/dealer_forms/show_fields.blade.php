<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $dealerForm->name }}</p>
</div>

<!-- Url Field -->
<div class="col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    <p>{{ $dealerForm->url }}</p>
</div>

<!-- Dealer Id Field -->
<div class="col-sm-12">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    <p>{{ $dealerForm->dealer_id }}</p>
</div>

