<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $buildingOrderNote->user_id }}</p>
</div>

<!-- Building Order Id Field -->
<div class="col-sm-12">
    {!! Form::label('building_order_id', 'Building Order Id:') !!}
    <p>{{ $buildingOrderNote->building_order_id }}</p>
</div>

<!-- Note Field -->
<div class="col-sm-12">
    {!! Form::label('note', 'Note:') !!}
    <p>{{ $buildingOrderNote->note }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $buildingOrderNote->type }}</p>
</div>

<!-- Document Field -->
<div class="col-sm-12">
    {!! Form::label('document', 'Document:') !!}
    <p>{{ $buildingOrderNote->document }}</p>
</div>

