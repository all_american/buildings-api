<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="building-order-notes-table">
            <thead>
            <tr>
                <th>User Id</th>
                <th>Building Order Id</th>
                <th>Note</th>
                <th>Type</th>
                <th>Document</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($buildingOrderNotes as $buildingOrderNote)
                <tr>
                    <td>{{ $buildingOrderNote->user_id }}</td>
                    <td>{{ $buildingOrderNote->building_order_id }}</td>
                    <td>{{ $buildingOrderNote->note }}</td>
                    <td>{{ $buildingOrderNote->type }}</td>
                    <td>{{ $buildingOrderNote->document }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['buildingOrderNotes.destroy', $buildingOrderNote->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('buildingOrderNotes.show', [$buildingOrderNote->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('buildingOrderNotes.edit', [$buildingOrderNote->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $buildingOrderNotes])
        </div>
    </div>
</div>
