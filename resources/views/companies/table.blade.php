<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="companies-table">
            <thead>
            <tr>
                <th>Full Name</th>
                <th>Abbreviation</th>
                <th>Address</th>
                <th>Country</th>
                <th>City</th>
                <th>State</th>
                <th>Zip Code</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Logo Color</th>
                <th>Logo White</th>
                <th>Website</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($companies as $company)
                <tr>
                    <td>{{ $company->full_name }}</td>
                    <td>{{ $company->abbreviation }}</td>
                    <td>{{ $company->address }}</td>
                    <td>{{ $company->country }}</td>
                    <td>{{ $company->city }}</td>
                    <td>{{ $company->state }}</td>
                    <td>{{ $company->zip_code }}</td>
                    <td>{{ $company->phone }}</td>
                    <td>{{ $company->email }}</td>
                    <td>{{ $company->logo_color }}</td>
                    <td>{{ $company->logo_white }}</td>
                    <td>{{ $company->website }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['companies.destroy', $company->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('companies.show', [$company->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('companies.edit', [$company->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $companies])
        </div>
    </div>
</div>
