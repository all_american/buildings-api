<!-- Full Name Field -->
<div class="col-sm-12">
    {!! Form::label('full_name', 'Full Name:') !!}
    <p>{{ $company->full_name }}</p>
</div>

<!-- Abbreviation Field -->
<div class="col-sm-12">
    {!! Form::label('abbreviation', 'Abbreviation:') !!}
    <p>{{ $company->abbreviation }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $company->address }}</p>
</div>

<!-- Country Field -->
<div class="col-sm-12">
    {!! Form::label('country', 'Country:') !!}
    <p>{{ $company->country }}</p>
</div>

<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', 'City:') !!}
    <p>{{ $company->city }}</p>
</div>

<!-- State Field -->
<div class="col-sm-12">
    {!! Form::label('state', 'State:') !!}
    <p>{{ $company->state }}</p>
</div>

<!-- Zip Code Field -->
<div class="col-sm-12">
    {!! Form::label('zip_code', 'Zip Code:') !!}
    <p>{{ $company->zip_code }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $company->phone }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $company->email }}</p>
</div>

<!-- Logo Color Field -->
<div class="col-sm-12">
    {!! Form::label('logo_color', 'Logo Color:') !!}
    <p>{{ $company->logo_color }}</p>
</div>

<!-- Logo White Field -->
<div class="col-sm-12">
    {!! Form::label('logo_white', 'Logo White:') !!}
    <p>{{ $company->logo_white }}</p>
</div>

<!-- Website Field -->
<div class="col-sm-12">
    {!! Form::label('website', 'Website:') !!}
    <p>{{ $company->website }}</p>
</div>

