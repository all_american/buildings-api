<!-- need to remove -->
<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link {{ Request::is('home') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Home</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('companies.index') }}" class="nav-link {{ Request::is('companies*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Companies</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('users.index') }}" class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Users</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('profiles.index') }}" class="nav-link {{ Request::is('profiles*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Profiles</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('notifications.index') }}" class="nav-link {{ Request::is('notifications*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Notifications</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('states.index') }}" class="nav-link {{ Request::is('states*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>States</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('dealers.index') }}" class="nav-link {{ Request::is('dealers*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Dealers</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('dealerAddresses.index') }}" class="nav-link {{ Request::is('dealerAddresses*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Dealer Addresses</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('dealerForms.index') }}" class="nav-link {{ Request::is('dealerForms*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Dealer Forms</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('dealerImages.index') }}" class="nav-link {{ Request::is('dealerImages*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Dealer Images</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('dealerHours.index') }}" class="nav-link {{ Request::is('dealerHours*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Dealer Hours</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('buildingOrders.index') }}" class="nav-link {{ Request::is('buildingOrders*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Building Orders</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('buildingOrderNotes.index') }}" class="nav-link {{ Request::is('buildingOrderNotes*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Building Order Notes</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('buildingOrderDetails.index') }}" class="nav-link {{ Request::is('buildingOrderDetails*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Building Order Details</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('statuses.index') }}" class="nav-link {{ Request::is('statuses*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Statuses</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('buildingOrderStatuses.index') }}" class="nav-link {{ Request::is('buildingOrderStatuses*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Building Order Statuses</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('repairs.index') }}" class="nav-link {{ Request::is('repairs*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Repairs</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('repairImages.index') }}" class="nav-link {{ Request::is('repairImages*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Repair Images</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('registers.index') }}" class="nav-link {{ Request::is('registers*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Registers</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('registers.index') }}" class="nav-link {{ Request::is('registers*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Registers</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('sellers.index') }}" class="nav-link {{ Request::is('sellers*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Sellers</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('manufactures.index') }}" class="nav-link {{ Request::is('manufactures*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Manufactures</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('registers.index') }}" class="nav-link {{ Request::is('registers*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Registers</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('manufactures.index') }}" class="nav-link {{ Request::is('manufactures*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Manufactures</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('sellers.index') }}" class="nav-link {{ Request::is('sellers*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Sellers</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('reportEmails.index') }}" class="nav-link {{ Request::is('reportEmails*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Report Emails</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('reports.index') }}" class="nav-link {{ Request::is('reports*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>Reports</p>
    </a>
</li>
