<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $buildingOrder->user_id }}</p>
</div>

<!-- Dealer Id Field -->
<div class="col-sm-12">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    <p>{{ $buildingOrder->dealer_id }}</p>
</div>

<!-- Invoice Number Field -->
<div class="col-sm-12">
    {!! Form::label('invoice_number', 'Invoice Number:') !!}
    <p>{{ $buildingOrder->invoice_number }}</p>
</div>

<!-- Invoice Date Field -->
<div class="col-sm-12">
    {!! Form::label('invoice_date', 'Invoice Date:') !!}
    <p>{{ $buildingOrder->invoice_date }}</p>
</div>

<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $buildingOrder->name }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $buildingOrder->email }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $buildingOrder->phone }}</p>
</div>

<!-- Phone2 Field -->
<div class="col-sm-12">
    {!! Form::label('phone2', 'Phone2:') !!}
    <p>{{ $buildingOrder->phone2 }}</p>
</div>

<!-- Phone3 Field -->
<div class="col-sm-12">
    {!! Form::label('phone3', 'Phone3:') !!}
    <p>{{ $buildingOrder->phone3 }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $buildingOrder->address }}</p>
</div>

<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', 'City:') !!}
    <p>{{ $buildingOrder->city }}</p>
</div>

<!-- State Id Field -->
<div class="col-sm-12">
    {!! Form::label('state_id', 'State Id:') !!}
    <p>{{ $buildingOrder->state_id }}</p>
</div>

<!-- Zip Field -->
<div class="col-sm-12">
    {!! Form::label('zip', 'Zip:') !!}
    <p>{{ $buildingOrder->zip }}</p>
</div>

<!-- Width Field -->
<div class="col-sm-12">
    {!! Form::label('width', 'Width:') !!}
    <p>{{ $buildingOrder->width }}</p>
</div>

<!-- Roof Length Field -->
<div class="col-sm-12">
    {!! Form::label('roof_length', 'Roof Length:') !!}
    <p>{{ $buildingOrder->roof_length }}</p>
</div>

<!-- Frame Length Field -->
<div class="col-sm-12">
    {!! Form::label('frame_length', 'Frame Length:') !!}
    <p>{{ $buildingOrder->frame_length }}</p>
</div>

<!-- Leg Height Field -->
<div class="col-sm-12">
    {!! Form::label('leg_height', 'Leg Height:') !!}
    <p>{{ $buildingOrder->leg_height }}</p>
</div>

<!-- Gauge Field -->
<div class="col-sm-12">
    {!! Form::label('gauge', 'Gauge:') !!}
    <p>{{ $buildingOrder->gauge }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $buildingOrder->price }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $buildingOrder->description }}</p>
</div>

<!-- Description Price Field -->
<div class="col-sm-12">
    {!! Form::label('description_price', 'Description Price:') !!}
    <p>{{ $buildingOrder->description_price }}</p>
</div>

<!-- Color Top Field -->
<div class="col-sm-12">
    {!! Form::label('color_top', 'Color Top:') !!}
    <p>{{ $buildingOrder->color_top }}</p>
</div>

<!-- Color Sides Field -->
<div class="col-sm-12">
    {!! Form::label('color_sides', 'Color Sides:') !!}
    <p>{{ $buildingOrder->color_sides }}</p>
</div>

<!-- Color Ends Field -->
<div class="col-sm-12">
    {!! Form::label('color_ends', 'Color Ends:') !!}
    <p>{{ $buildingOrder->color_ends }}</p>
</div>

<!-- Color Trim Field -->
<div class="col-sm-12">
    {!! Form::label('color_trim', 'Color Trim:') !!}
    <p>{{ $buildingOrder->color_trim }}</p>
</div>

<!-- Non Tax Fees Desc Field -->
<div class="col-sm-12">
    {!! Form::label('non_tax_fees_desc', 'Non Tax Fees Desc:') !!}
    <p>{{ $buildingOrder->non_tax_fees_desc }}</p>
</div>

<!-- Non Tax Fees Field -->
<div class="col-sm-12">
    {!! Form::label('non_tax_fees', 'Non Tax Fees:') !!}
    <p>{{ $buildingOrder->non_tax_fees }}</p>
</div>

<!-- Installation Address Field -->
<div class="col-sm-12">
    {!! Form::label('installation_address', 'Installation Address:') !!}
    <p>{{ $buildingOrder->installation_address }}</p>
</div>

<!-- Installation City Field -->
<div class="col-sm-12">
    {!! Form::label('installation_city', 'Installation City:') !!}
    <p>{{ $buildingOrder->installation_city }}</p>
</div>

<!-- Installation State Id Field -->
<div class="col-sm-12">
    {!! Form::label('installation_state_id', 'Installation State Id:') !!}
    <p>{{ $buildingOrder->installation_state_id }}</p>
</div>

<!-- Installation Zip Field -->
<div class="col-sm-12">
    {!! Form::label('installation_zip', 'Installation Zip:') !!}
    <p>{{ $buildingOrder->installation_zip }}</p>
</div>

<!-- Total Tax Field -->
<div class="col-sm-12">
    {!! Form::label('total_tax', 'Total Tax:') !!}
    <p>{{ $buildingOrder->total_tax }}</p>
</div>

<!-- Total Sales Field -->
<div class="col-sm-12">
    {!! Form::label('total_sales', 'Total Sales:') !!}
    <p>{{ $buildingOrder->total_sales }}</p>
</div>

<!-- Tax Field -->
<div class="col-sm-12">
    {!! Form::label('tax', 'Tax:') !!}
    <p>{{ $buildingOrder->tax }}</p>
</div>

<!-- Tax Exempt Field -->
<div class="col-sm-12">
    {!! Form::label('tax_exempt', 'Tax Exempt:') !!}
    <p>{{ $buildingOrder->tax_exempt }}</p>
</div>

<!-- Total Field -->
<div class="col-sm-12">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $buildingOrder->total }}</p>
</div>

<!-- Deposit Field -->
<div class="col-sm-12">
    {!! Form::label('deposit', 'Deposit:') !!}
    <p>{{ $buildingOrder->deposit }}</p>
</div>

<!-- Balance Due Field -->
<div class="col-sm-12">
    {!! Form::label('balance_due', 'Balance Due:') !!}
    <p>{{ $buildingOrder->balance_due }}</p>
</div>

<!-- Special Instructions Field -->
<div class="col-sm-12">
    {!! Form::label('special_instructions', 'Special Instructions:') !!}
    <p>{{ $buildingOrder->special_instructions }}</p>
</div>

<!-- Surface Level Field -->
<div class="col-sm-12">
    {!! Form::label('surface_level', 'Surface Level:') !!}
    <p>{{ $buildingOrder->surface_level }}</p>
</div>

<!-- Electricity Available Field -->
<div class="col-sm-12">
    {!! Form::label('electricity_available', 'Electricity Available:') !!}
    <p>{{ $buildingOrder->electricity_available }}</p>
</div>

<!-- Installation Surface Field -->
<div class="col-sm-12">
    {!! Form::label('installation_surface', 'Installation Surface:') !!}
    <p>{{ $buildingOrder->installation_surface }}</p>
</div>

<!-- Payment Field -->
<div class="col-sm-12">
    {!! Form::label('payment', 'Payment:') !!}
    <p>{{ $buildingOrder->payment }}</p>
</div>

<!-- Collection Field -->
<div class="col-sm-12">
    {!! Form::label('collection', 'Collection:') !!}
    <p>{{ $buildingOrder->collection }}</p>
</div>

<!-- Ready Install Field -->
<div class="col-sm-12">
    {!! Form::label('ready_install', 'Ready Install:') !!}
    <p>{{ $buildingOrder->ready_install }}</p>
</div>

<!-- Latitude Field -->
<div class="col-sm-12">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{{ $buildingOrder->latitude }}</p>
</div>

<!-- Longitude Field -->
<div class="col-sm-12">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{{ $buildingOrder->longitude }}</p>
</div>

