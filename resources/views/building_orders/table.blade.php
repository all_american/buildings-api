<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="building-orders-table">
            <thead>
            <tr>
                <th>User Id</th>
                <th>Dealer Id</th>
                <th>Invoice Number</th>
                <th>Invoice Date</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Phone2</th>
                <th>Phone3</th>
                <th>Address</th>
                <th>City</th>
                <th>State Id</th>
                <th>Zip</th>
                <th>Width</th>
                <th>Roof Length</th>
                <th>Frame Length</th>
                <th>Leg Height</th>
                <th>Gauge</th>
                <th>Price</th>
                <th>Description</th>
                <th>Description Price</th>
                <th>Color Top</th>
                <th>Color Sides</th>
                <th>Color Ends</th>
                <th>Color Trim</th>
                <th>Non Tax Fees Desc</th>
                <th>Non Tax Fees</th>
                <th>Installation Address</th>
                <th>Installation City</th>
                <th>Installation State Id</th>
                <th>Installation Zip</th>
                <th>Total Tax</th>
                <th>Total Sales</th>
                <th>Tax</th>
                <th>Tax Exempt</th>
                <th>Total</th>
                <th>Deposit</th>
                <th>Balance Due</th>
                <th>Special Instructions</th>
                <th>Surface Level</th>
                <th>Electricity Available</th>
                <th>Installation Surface</th>
                <th>Payment</th>
                <th>Collection</th>
                <th>Ready Install</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($buildingOrders as $buildingOrder)
                <tr>
                    <td>{{ $buildingOrder->user_id }}</td>
                    <td>{{ $buildingOrder->dealer_id }}</td>
                    <td>{{ $buildingOrder->invoice_number }}</td>
                    <td>{{ $buildingOrder->invoice_date }}</td>
                    <td>{{ $buildingOrder->name }}</td>
                    <td>{{ $buildingOrder->email }}</td>
                    <td>{{ $buildingOrder->phone }}</td>
                    <td>{{ $buildingOrder->phone2 }}</td>
                    <td>{{ $buildingOrder->phone3 }}</td>
                    <td>{{ $buildingOrder->address }}</td>
                    <td>{{ $buildingOrder->city }}</td>
                    <td>{{ $buildingOrder->state_id }}</td>
                    <td>{{ $buildingOrder->zip }}</td>
                    <td>{{ $buildingOrder->width }}</td>
                    <td>{{ $buildingOrder->roof_length }}</td>
                    <td>{{ $buildingOrder->frame_length }}</td>
                    <td>{{ $buildingOrder->leg_height }}</td>
                    <td>{{ $buildingOrder->gauge }}</td>
                    <td>{{ $buildingOrder->price }}</td>
                    <td>{{ $buildingOrder->description }}</td>
                    <td>{{ $buildingOrder->description_price }}</td>
                    <td>{{ $buildingOrder->color_top }}</td>
                    <td>{{ $buildingOrder->color_sides }}</td>
                    <td>{{ $buildingOrder->color_ends }}</td>
                    <td>{{ $buildingOrder->color_trim }}</td>
                    <td>{{ $buildingOrder->non_tax_fees_desc }}</td>
                    <td>{{ $buildingOrder->non_tax_fees }}</td>
                    <td>{{ $buildingOrder->installation_address }}</td>
                    <td>{{ $buildingOrder->installation_city }}</td>
                    <td>{{ $buildingOrder->installation_state_id }}</td>
                    <td>{{ $buildingOrder->installation_zip }}</td>
                    <td>{{ $buildingOrder->total_tax }}</td>
                    <td>{{ $buildingOrder->total_sales }}</td>
                    <td>{{ $buildingOrder->tax }}</td>
                    <td>{{ $buildingOrder->tax_exempt }}</td>
                    <td>{{ $buildingOrder->total }}</td>
                    <td>{{ $buildingOrder->deposit }}</td>
                    <td>{{ $buildingOrder->balance_due }}</td>
                    <td>{{ $buildingOrder->special_instructions }}</td>
                    <td>{{ $buildingOrder->surface_level }}</td>
                    <td>{{ $buildingOrder->electricity_available }}</td>
                    <td>{{ $buildingOrder->installation_surface }}</td>
                    <td>{{ $buildingOrder->payment }}</td>
                    <td>{{ $buildingOrder->collection }}</td>
                    <td>{{ $buildingOrder->ready_install }}</td>
                    <td>{{ $buildingOrder->latitude }}</td>
                    <td>{{ $buildingOrder->longitude }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['buildingOrders.destroy', $buildingOrder->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('buildingOrders.show', [$buildingOrder->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('buildingOrders.edit', [$buildingOrder->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $buildingOrders])
        </div>
    </div>
</div>
