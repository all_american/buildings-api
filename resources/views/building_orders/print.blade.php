<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Order</title>
    <style>
        @charset "UTF-8";:root{--bs-blue:#0d6efd;--bs-indigo:#6610f2;--bs-purple:#6f42c1;--bs-pink:#d63384;--bs-red:#dc3545;--bs-orange:#fd7e14;--bs-yellow:#ffc107;--bs-green:#198754;--bs-teal:#20c997;--bs-cyan:#0dcaf0;--bs-black:#000;--bs-white:#fff;--bs-gray:#6c757d;--bs-gray-dark:#343a40;--bs-gray-100:#f8f9fa;--bs-gray-200:#e9ecef;--bs-gray-300:#dee2e6;--bs-gray-400:#ced4da;--bs-gray-500:#adb5bd;--bs-gray-600:#6c757d;--bs-gray-700:#495057;--bs-gray-800:#343a40;--bs-gray-900:#212529;--bs-primary:#0d6efd;--bs-secondary:#6c757d;--bs-success:#198754;--bs-info:#0dcaf0;--bs-warning:#ffc107;--bs-danger:#dc3545;--bs-light:#f8f9fa;--bs-dark:#212529;--bs-primary-rgb:13,110,253;--bs-secondary-rgb:108,117,125;--bs-success-rgb:25,135,84;--bs-info-rgb:13,202,240;--bs-warning-rgb:255,193,7;--bs-danger-rgb:220,53,69;--bs-light-rgb:248,249,250;--bs-dark-rgb:33,37,41;--bs-white-rgb:255,255,255;--bs-black-rgb:0,0,0;--bs-body-color-rgb:33,37,41;--bs-body-bg-rgb:255,255,255;--bs-font-sans-serif:system-ui,-apple-system,"Segoe UI",Roboto,"Helvetica Neue","Noto Sans","Liberation Sans",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--bs-font-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;--bs-gradient:linear-gradient(180deg, rgba(255, 255, 255, 0.15), rgba(255, 255, 255, 0));--bs-body-font-family:var(--bs-font-sans-serif);--bs-body-font-size:1rem;--bs-body-font-weight:400;--bs-body-line-height:1.5;--bs-body-color:#212529;--bs-body-bg:#fff;--bs-border-width:1px;--bs-border-style:solid;--bs-border-color:#dee2e6;--bs-border-color-translucent:rgba(0, 0, 0, 0.175);--bs-border-radius:0.375rem;--bs-border-radius-sm:0.25rem;--bs-border-radius-lg:0.5rem;--bs-border-radius-xl:1rem;--bs-border-radius-2xl:2rem;--bs-border-radius-pill:50rem;--bs-link-color:#0d6efd;--bs-link-hover-color:#0a58ca;--bs-code-color:#d63384;--bs-highlight-bg:#fff3cd}*,::after,::before{box-sizing:border-box}@media (prefers-reduced-motion:no-preference){:root{scroll-behavior:smooth}}body{margin:0;font-family:var(--bs-body-font-family);font-size:var(--bs-body-font-size);font-weight:var(--bs-body-font-weight);line-height:var(--bs-body-line-height);color:var(--bs-body-color);text-align:var(--bs-body-text-align);background-color:var(--bs-body-bg);-webkit-text-size-adjust:100%}p{margin-top:0;margin-bottom:1rem}strong{font-weight:bolder}img{vertical-align:middle}table{caption-side:bottom;border-collapse:collapse}th{text-align:inherit;text-align:-webkit-match-parent}tbody,td,th,thead,tr{border-color:inherit;border-style:solid;border-width:0}label{display:inline-block}input{margin:0;font-family:inherit;font-size:inherit;line-height:inherit}::-moz-focus-inner{padding:0;border-style:none}::-webkit-datetime-edit-day-field,::-webkit-datetime-edit-fields-wrapper,::-webkit-datetime-edit-hour-field,::-webkit-datetime-edit-minute,::-webkit-datetime-edit-month-field,::-webkit-datetime-edit-text,::-webkit-datetime-edit-year-field{padding:0}::-webkit-inner-spin-button{height:auto}::-webkit-search-decoration{-webkit-appearance:none}::-webkit-color-swatch-wrapper{padding:0}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}::file-selector-button{font:inherit;-webkit-appearance:button}.container{--bs-gutter-x:1.5rem;--bs-gutter-y:0;width:100%;padding-right:calc(var(--bs-gutter-x) * .5);padding-left:calc(var(--bs-gutter-x) * .5);margin-right:auto;margin-left:auto}@media (min-width:576px){.container{max-width:540px}}@media (min-width:768px){.container{max-width:720px}}@media (min-width:992px){.container{max-width:960px}}@media (min-width:1200px){.container{max-width:1140px}}@media (min-width:1400px){.container{max-width:1320px}}.row{--bs-gutter-x:1.5rem;--bs-gutter-y:0;display:flex;flex-wrap:wrap;margin-top:calc(-1 * var(--bs-gutter-y));margin-right:calc(-.5 * var(--bs-gutter-x));margin-left:calc(-.5 * var(--bs-gutter-x))}.row>*{flex-shrink:0;width:100%;max-width:100%;padding-right:calc(var(--bs-gutter-x) * .5);padding-left:calc(var(--bs-gutter-x) * .5);margin-top:var(--bs-gutter-y)}.col-2{flex:0 0 auto;width:16.66666667%}.col-3{flex:0 0 auto;width:25%}.col-4{flex:0 0 auto;width:33.33333333%}.col-5{flex:0 0 auto;width:41.66666667%}.col-6{flex:0 0 auto;width:50%}.col-7{flex:0 0 auto;width:58.33333333%}.col-8{flex:0 0 auto;width:66.66666667%}.col-9{flex:0 0 auto;width:75%}.col-10{flex:0 0 auto;width:83.33333333%}.col-12{flex:0 0 auto;width:100%}.table{--bs-table-color:var(--bs-body-color);--bs-table-bg:transparent;--bs-table-border-color:var(--bs-border-color);--bs-table-accent-bg:transparent;--bs-table-striped-color:var(--bs-body-color);--bs-table-striped-bg:rgba(0, 0, 0, 0.05);--bs-table-active-color:var(--bs-body-color);--bs-table-active-bg:rgba(0, 0, 0, 0.1);--bs-table-hover-color:var(--bs-body-color);--bs-table-hover-bg:rgba(0, 0, 0, 0.075);width:100%;margin-bottom:1rem;color:var(--bs-table-color);vertical-align:top;border-color:var(--bs-table-border-color)}.table>:not(caption)>*>*{padding:.5rem .5rem;background-color:var(--bs-table-bg);border-bottom-width:1px;box-shadow:inset 0 0 0 9999px var(--bs-table-accent-bg)}.table>tbody{vertical-align:inherit}.table>thead{vertical-align:bottom}.table-bordered>:not(caption)>*{border-width:1px 0}.table-bordered>:not(caption)>*>*{border-width:0 1px}.table-light{--bs-table-color:#000;--bs-table-bg:#f8f9fa;--bs-table-border-color:#dfe0e1;--bs-table-striped-bg:#ecedee;--bs-table-striped-color:#000;--bs-table-active-bg:#dfe0e1;--bs-table-active-color:#000;--bs-table-hover-bg:#e5e6e7;--bs-table-hover-color:#000;color:var(--bs-table-color);border-color:var(--bs-table-border-color)}.col-form-label{padding-top:calc(.375rem + 1px);padding-bottom:calc(.375rem + 1px);margin-bottom:0;font-size:inherit;line-height:1.5}.form-control{display:block;width:100%;padding:.375rem .75rem;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;background-color:#fff;background-clip:padding-box;border:1px solid #ced4da;-webkit-appearance:none;-moz-appearance:none;appearance:none;border-radius:.375rem}.form-control::-webkit-date-and-time-value{height:1.5em}.form-control::-moz-placeholder{color:#6c757d;opacity:1}.form-control::-webkit-file-upload-button{padding:.375rem .75rem;margin:-.375rem -.75rem;-webkit-margin-end:.75rem;margin-inline-end:.75rem;color:#212529;background-color:#e9ecef;border-color:inherit;border-style:solid;border-width:0;border-inline-end-width:1px;border-radius:0}.form-control-sm{min-height:calc(1.5em + .5rem + 2px);padding:.25rem .5rem;font-size:.875rem;border-radius:.25rem}.form-control-sm::-webkit-file-upload-button{padding:.25rem .5rem;margin:-.25rem -.5rem;-webkit-margin-end:.5rem;margin-inline-end:.5rem}.mt-2{margin-top:.5rem!important}.mt-4{margin-top:1.5rem!important}.mb-3{margin-bottom:1rem!important}.text-center{text-align:center!important}
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-4">
            <img src="https://storage.googleapis.com/allamericanmbs/assets/fc%20Steel%20Structures-Color.png" alt=""
                 width="50%">
        </div>
        <div class="col-4 mt-2">
            <p class="text-center"><strong>Invoice: </strong><br><strong>Order Date: </strong><br><strong>Install
                    Date: </strong><br>Address 200 Olive St - (336) 755-1494</p>
        </div>
        <div class="col-4 mt-4">
            <p class="text-center">Deposit Valid <br>For 1Year!</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p>Order Status: <strong>Quote</strong></p>
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-7">
            <div class="mb-3 row">
                <label for="dealer" class="col-2 col-form-label">Dealer:</label>
                <div class="col-10">
                    <input type="text" class="form-control form-control-sm" id="dealer">
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="mb-3 row">
                <label for="phone" class="col-3 col-form-label">Phone:</label>
                <div class="col-9">
                    <input type="text" class="form-control form-control-sm" id="phone">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="mb-3 row">
                <label for="buyer" class="col-2 col-form-label">Buyer:</label>
                <div class="col-10">
                    <input type="text" class="form-control form-control-sm" id="buyer">
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="mb-3 row">
                <label for="email" class="col-3 col-form-label">Email:</label>
                <div class="col-9">
                    <input type="text" class="form-control form-control-sm" id="email">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="mb-3 row">
                <label for="ins_add" class="col-3 col-form-label">Install address:</label>
                <div class="col-9">
                    <input type="text" class="form-control form-control-sm" id="ins_add">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="mb-3 row">
                <label for="city" class="col-4 col-form-label">city:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="city">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="mb-3 row">
                <label for="state" class="col-4 col-form-label">State:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="state">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="mb-3 row">
                <label for="zip" class="col-4 col-form-label">Zip:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="zip">
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="mb-3 row">
                <label for="cell" class="col-4 col-form-label">Cell:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="cell">
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="mb-3 row">
                <label for="country" class="col-4 col-form-label">Country:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="country">
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="mb-3 row">
                <label for="phone_client" class="col-4 col-form-label">Phone:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="phone_client">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <div class="mb-3 row">
                <label for="width" class="col-4 col-form-label">Width:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="width">
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="mb-3 row">
                <label for="roof" class="col-4 col-form-label">Roof:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="roof">
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="mb-3 row">
                <label for="frame" class="col-4 col-form-label">Frame:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="frame">
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="mb-3 row">
                <label for="height" class="col-4 col-form-label">Height:</label>
                <div class="col-8">
                    <input type="text" class="form-control form-control-sm" id="height">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <table class="table table-bordered">
                <thead class="table-light">
                <tr>
                    <th scope="col">Description</th>
                    <th scope="col">Price</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td></td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Non TaxFees:</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-4">
            <table class="table table-bordered">
                <thead class="table-light">
                <tr>
                    <th colspan="2">ALL ORDERS ARE P.O.D.</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Total Sale:</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Tax:</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>TaxExempt#:</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Non TaxFees:</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Total:</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Deposit:</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Balance Due:</td>
                    <td>0</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="row">
                <div class="col-12">
                    <p>Colors:</p>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="roof_color" class="col-6 col-form-label">Roof:</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="roof_color">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="surface" class="col-6 col-form-label">Is your surface level?</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="surface">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="sides_color" class="col-6 col-form-label">Sides:</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="sides_color">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="electricity" class="col-6 col-form-label">Electricity?</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="electricity">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="Ends_color" class="col-6 col-form-label">Ends:</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="Ends_color">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="inst_surface" class="col-6 col-form-label">Installation Surface</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="inst_surface">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="trim_color" class="col-6 col-form-label">Trim:</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="trim_color">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3 row">
                        <label for="payment" class="col-6 col-form-label">Payment Method:</label>
                        <div class="col-6">
                            <input type="text" class="form-control form-control-sm" id="payment">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <table class="table table-bordered">
                <thead class="table-light">
                <tr>
                    <th>Special Instructions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <p><strong>ThingsYouShouldKnow....</strong><br>
            FC Steel Structures. Is not responsible for permits, covenant searches or restrictions. Please contact your
            local Building Inspector or Homeowners Association for information and notify
            our company of any requirements for which we must comply. The installation site must be level to qualify for
            our free installation, if it is not, it is the buyer's responsibility to cover any
            additional labor and/ or provide the materials necessary to make it so.Prior to installation, please have
            any underground cables, gas lines, or any other utility lines located and marked. FC
            Steel Structures will not be responsible for any damage to underground utility lines. NO MORE than the
            required deposit is to be paid to the dealer! FC Steel Structures will not be
            responsible for any monies paid to the dealer above the required deposit amount and such payments will delay
            the process of your order until those funds have been paid in-full to FC
            Steel Structures by the dealer. Paying in full to the dealer will not speed up your installation date. ALL
            DEPOSITSARE NON-REFUNDABLE.Please be advised that installation times are
            subject to change due to contractor availability and weather conditions. IF the contractor is unable to
            complete your job due to site complications, or inaccurate measurements a $100
            return fee (non-tax) will apply. Any returned checks or refunds for over-payments made by credit card will
            incur a 5% fee.<br><br>
            Buyer agrees that the balance shall be due and payable in full at the time of installation. If balances due
            and owing at the time of installation are not paid in full, buyer shall be in default
            under this agreement and subject to a $50 late fee. At its sole discretion FC Steel Structures may
            assessinterest at a rate of 18% per annum on any unpaid balance. In the event of any
            unpaid balance buyer agrees to allow FCSteel Structures access to the property to repossess the building.
            Buyer agrees that in the event of any default under this agreement, buyer shall
            be responsible for reasonable collection agency costs, any attorney's fees and cost incurred because of the
            default. JURISDICTION,it is expressly agreed that in any dispute, suit, claim or
            legal proceeding of any nature arising from the obligations under this agreement, shall be filed in a court
            of competent jurisdiction. FCSteel Structures reserves the right to terminate this
            agreement at any time. FCSteel Structures must approve all pricing before contract is valid.
            I have read and completely understandthe above informationand give my approvalfor constructionof the above
        </p>
    </div>
    <div class="col-12" style="margin-top: 3%;">
        <div style="display: flex; justify-content: space-between;">
            <span>Buyer Signature:_____________________________</span>
            <span style="margin-left: -10%;">Date:_______________________</span>
        </div>
    </div>
    <div class="col-12" style="margin-top: 3%;">
        <div style="display: flex; justify-content: space-between;">
            <span>Contractor Name:_____________________________</span>
            <span style="margin-left: -10%;">Date:_______________________</span>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3"
        crossorigin="anonymous"></script>
</body>
</html>
