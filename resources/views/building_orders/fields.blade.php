<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Dealer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    {!! Form::number('dealer_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Invoice Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('invoice_number', 'Invoice Number:') !!}
    {!! Form::number('invoice_number', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Invoice Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('invoice_date', 'Invoice Date:') !!}
    {!! Form::text('invoice_date', null, ['class' => 'form-control','id'=>'invoice_date']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#invoice_date').datepicker()
    </script>
@endpush

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Phone2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone2', 'Phone2:') !!}
    {!! Form::text('phone2', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone3 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone3', 'Phone3:') !!}
    {!! Form::text('phone3', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- State Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state_id', 'State Id:') !!}
    {!! Form::number('state_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Zip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zip', 'Zip:') !!}
    {!! Form::text('zip', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Width Field -->
<div class="form-group col-sm-6">
    {!! Form::label('width', 'Width:') !!}
    {!! Form::number('width', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Roof Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('roof_length', 'Roof Length:') !!}
    {!! Form::number('roof_length', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Frame Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('frame_length', 'Frame Length:') !!}
    {!! Form::number('frame_length', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Leg Height Field -->
<div class="form-group col-sm-6">
    {!! Form::label('leg_height', 'Leg Height:') !!}
    {!! Form::number('leg_height', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Gauge Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gauge', 'Gauge:') !!}
    {!! Form::text('gauge', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Description Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description_price', 'Description Price:') !!}
    {!! Form::number('description_price', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Color Top Field -->
<div class="form-group col-sm-6">
    {!! Form::label('color_top', 'Color Top:') !!}
    {!! Form::text('color_top', null, ['class' => 'form-control']) !!}
</div>

<!-- Color Sides Field -->
<div class="form-group col-sm-6">
    {!! Form::label('color_sides', 'Color Sides:') !!}
    {!! Form::text('color_sides', null, ['class' => 'form-control']) !!}
</div>

<!-- Color Ends Field -->
<div class="form-group col-sm-6">
    {!! Form::label('color_ends', 'Color Ends:') !!}
    {!! Form::text('color_ends', null, ['class' => 'form-control']) !!}
</div>

<!-- Color Trim Field -->
<div class="form-group col-sm-6">
    {!! Form::label('color_trim', 'Color Trim:') !!}
    {!! Form::text('color_trim', null, ['class' => 'form-control']) !!}
</div>

<!-- Non Tax Fees Desc Field -->
<div class="form-group col-sm-6">
    {!! Form::label('non_tax_fees_desc', 'Non Tax Fees Desc:') !!}
    {!! Form::text('non_tax_fees_desc', null, ['class' => 'form-control']) !!}
</div>

<!-- Non Tax Fees Field -->
<div class="form-group col-sm-6">
    {!! Form::label('non_tax_fees', 'Non Tax Fees:') !!}
    {!! Form::number('non_tax_fees', null, ['class' => 'form-control']) !!}
</div>

<!-- Installation Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('installation_address', 'Installation Address:') !!}
    {!! Form::text('installation_address', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Installation City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('installation_city', 'Installation City:') !!}
    {!! Form::text('installation_city', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Installation State Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('installation_state_id', 'Installation State Id:') !!}
    {!! Form::number('installation_state_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Installation Zip Field -->
<div class="form-group col-sm-6">
    {!! Form::label('installation_zip', 'Installation Zip:') !!}
    {!! Form::text('installation_zip', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Total Tax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_tax', 'Total Tax:') !!}
    {!! Form::number('total_tax', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Total Sales Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total_sales', 'Total Sales:') !!}
    {!! Form::number('total_sales', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Tax Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax', 'Tax:') !!}
    {!! Form::number('tax', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Tax Exempt Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tax_exempt', 'Tax Exempt:') !!}
    {!! Form::text('tax_exempt', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Deposit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deposit', 'Deposit:') !!}
    {!! Form::number('deposit', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Balance Due Field -->
<div class="form-group col-sm-6">
    {!! Form::label('balance_due', 'Balance Due:') !!}
    {!! Form::number('balance_due', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Special Instructions Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('special_instructions', 'Special Instructions:') !!}
    {!! Form::textarea('special_instructions', null, ['class' => 'form-control']) !!}
</div>

<!-- Surface Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('surface_level', 'Surface Level:') !!}
    {!! Form::text('surface_level', null, ['class' => 'form-control']) !!}
</div>

<!-- Electricity Available Field -->
<div class="form-group col-sm-6">
    {!! Form::label('electricity_available', 'Electricity Available:') !!}
    {!! Form::text('electricity_available', null, ['class' => 'form-control']) !!}
</div>

<!-- Installation Surface Field -->
<div class="form-group col-sm-6">
    {!! Form::label('installation_surface', 'Installation Surface:') !!}
    {!! Form::text('installation_surface', null, ['class' => 'form-control']) !!}
</div>

<!-- Payment Field -->
<div class="form-group col-sm-6">
    {!! Form::label('payment', 'Payment:') !!}
    {!! Form::text('payment', null, ['class' => 'form-control']) !!}
</div>

<!-- Collection Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('collection', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('collection', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('collection', 'Collection', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Ready Install Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ready_install', 'Ready Install:') !!}
    {!! Form::text('ready_install', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
</div>