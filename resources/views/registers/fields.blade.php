<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Build Cost Field -->
<div class="form-group col-sm-6">
    {!! Form::label('build_cost', 'Build Cost:') !!}
    {!! Form::number('build_cost', null, ['class' => 'form-control']) !!}
</div>

<!-- Document Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('document_path', 'Document Path:') !!}
    {!! Form::text('document_path', null, ['class' => 'form-control']) !!}
</div>

<!-- Deposit Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deposit', 'Deposit:') !!}
    {!! Form::number('deposit', null, ['class' => 'form-control']) !!}
</div>

<!-- Installation Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('installation_date', 'Installation Date:') !!}
    {!! Form::text('installation_date', null, ['class' => 'form-control','id'=>'installation_date']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#installation_date').datepicker()
    </script>
@endpush

<!-- Status Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('status', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('status', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('status', 'Status', ['class' => 'form-check-label']) !!}
    </div>
</div>

<!-- Confirmation Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('confirmation_number', 'Confirmation Number:') !!}
    {!! Form::text('confirmation_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Zip Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zip_code', 'Zip Code:') !!}
    {!! Form::text('zip_code', null, ['class' => 'form-control']) !!}
</div>

<!-- State Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state_id', 'State Id:') !!}
    {!! Form::number('state_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Seller Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    {!! Form::number('seller_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Manufacture Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('manufacture_id', 'Manufacture Id:') !!}
    {!! Form::number('manufacture_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::textarea('note', null, ['class' => 'form-control']) !!}
</div>