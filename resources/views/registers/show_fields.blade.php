<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $register->name }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $register->phone }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $register->email }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $register->address }}</p>
</div>

<!-- Build Cost Field -->
<div class="col-sm-12">
    {!! Form::label('build_cost', 'Build Cost:') !!}
    <p>{{ $register->build_cost }}</p>
</div>

<!-- Document Path Field -->
<div class="col-sm-12">
    {!! Form::label('document_path', 'Document Path:') !!}
    <p>{{ $register->document_path }}</p>
</div>

<!-- Deposit Field -->
<div class="col-sm-12">
    {!! Form::label('deposit', 'Deposit:') !!}
    <p>{{ $register->deposit }}</p>
</div>

<!-- Installation Date Field -->
<div class="col-sm-12">
    {!! Form::label('installation_date', 'Installation Date:') !!}
    <p>{{ $register->installation_date }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $register->status }}</p>
</div>

<!-- Confirmation Number Field -->
<div class="col-sm-12">
    {!! Form::label('confirmation_number', 'Confirmation Number:') !!}
    <p>{{ $register->confirmation_number }}</p>
</div>

<!-- Zip Code Field -->
<div class="col-sm-12">
    {!! Form::label('zip_code', 'Zip Code:') !!}
    <p>{{ $register->zip_code }}</p>
</div>

<!-- State Id Field -->
<div class="col-sm-12">
    {!! Form::label('state_id', 'State Id:') !!}
    <p>{{ $register->state_id }}</p>
</div>

<!-- Seller Id Field -->
<div class="col-sm-12">
    {!! Form::label('seller_id', 'Seller Id:') !!}
    <p>{{ $register->seller_id }}</p>
</div>

<!-- Manufacture Id Field -->
<div class="col-sm-12">
    {!! Form::label('manufacture_id', 'Manufacture Id:') !!}
    <p>{{ $register->manufacture_id }}</p>
</div>

<!-- Note Field -->
<div class="col-sm-12">
    {!! Form::label('note', 'Note:') !!}
    <p>{{ $register->note }}</p>
</div>

