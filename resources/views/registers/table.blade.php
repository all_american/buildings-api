<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="registers-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Address</th>
                <th>Build Cost</th>
                <th>Document Path</th>
                <th>Deposit</th>
                <th>Installation Date</th>
                <th>Status</th>
                <th>Confirmation Number</th>
                <th>Zip Code</th>
                <th>State Id</th>
                <th>Seller Id</th>
                <th>Manufacture Id</th>
                <th>Note</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($registers as $register)
                <tr>
                    <td>{{ $register->name }}</td>
                    <td>{{ $register->phone }}</td>
                    <td>{{ $register->email }}</td>
                    <td>{{ $register->address }}</td>
                    <td>{{ $register->build_cost }}</td>
                    <td>{{ $register->document_path }}</td>
                    <td>{{ $register->deposit }}</td>
                    <td>{{ $register->installation_date }}</td>
                    <td>{{ $register->status }}</td>
                    <td>{{ $register->confirmation_number }}</td>
                    <td>{{ $register->zip_code }}</td>
                    <td>{{ $register->state_id }}</td>
                    <td>{{ $register->seller_id }}</td>
                    <td>{{ $register->manufacture_id }}</td>
                    <td>{{ $register->note }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['registers.destroy', $register->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('registers.show', [$register->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('registers.edit', [$register->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $registers])
        </div>
    </div>
</div>
