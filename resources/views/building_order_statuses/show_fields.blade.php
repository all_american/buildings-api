<!-- Building Order Id Field -->
<div class="col-sm-12">
    {!! Form::label('building_order_id', 'Building Order Id:') !!}
    <p>{{ $buildingOrderStatus->building_order_id }}</p>
</div>

<!-- Status Id Field -->
<div class="col-sm-12">
    {!! Form::label('status_id', 'Status Id:') !!}
    <p>{{ $buildingOrderStatus->status_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $buildingOrderStatus->user_id }}</p>
</div>

<!-- Note Field -->
<div class="col-sm-12">
    {!! Form::label('note', 'Note:') !!}
    <p>{{ $buildingOrderStatus->note }}</p>
</div>

