<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="building-order-statuses-table">
            <thead>
            <tr>
                <th>Building Order Id</th>
                <th>Status Id</th>
                <th>User Id</th>
                <th>Note</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($buildingOrderStatuses as $buildingOrderStatus)
                <tr>
                    <td>{{ $buildingOrderStatus->building_order_id }}</td>
                    <td>{{ $buildingOrderStatus->status_id }}</td>
                    <td>{{ $buildingOrderStatus->user_id }}</td>
                    <td>{{ $buildingOrderStatus->note }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['buildingOrderStatuses.destroy', $buildingOrderStatus->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('buildingOrderStatuses.show', [$buildingOrderStatus->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('buildingOrderStatuses.edit', [$buildingOrderStatus->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $buildingOrderStatuses])
        </div>
    </div>
</div>
