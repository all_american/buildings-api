<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="dealer-addresses-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>State Id</th>
                <th>Zip Code</th>
                <th>Dealer Id</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dealerAddresses as $dealerAddress)
                <tr>
                    <td>{{ $dealerAddress->name }}</td>
                    <td>{{ $dealerAddress->address }}</td>
                    <td>{{ $dealerAddress->city }}</td>
                    <td>{{ $dealerAddress->state_id }}</td>
                    <td>{{ $dealerAddress->zip_code }}</td>
                    <td>{{ $dealerAddress->dealer_id }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['dealerAddresses.destroy', $dealerAddress->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('dealerAddresses.show', [$dealerAddress->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('dealerAddresses.edit', [$dealerAddress->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $dealerAddresses])
        </div>
    </div>
</div>
