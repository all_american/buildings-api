<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $dealerAddress->name }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-12">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $dealerAddress->address }}</p>
</div>

<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', 'City:') !!}
    <p>{{ $dealerAddress->city }}</p>
</div>

<!-- State Id Field -->
<div class="col-sm-12">
    {!! Form::label('state_id', 'State Id:') !!}
    <p>{{ $dealerAddress->state_id }}</p>
</div>

<!-- Zip Code Field -->
<div class="col-sm-12">
    {!! Form::label('zip_code', 'Zip Code:') !!}
    <p>{{ $dealerAddress->zip_code }}</p>
</div>

<!-- Dealer Id Field -->
<div class="col-sm-12">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    <p>{{ $dealerAddress->dealer_id }}</p>
</div>

