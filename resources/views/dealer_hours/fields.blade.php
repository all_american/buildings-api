<!-- Day Field -->
<div class="form-group col-sm-6">
    {!! Form::label('day', 'Day:') !!}
    {!! Form::text('day', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Open Field -->
<div class="form-group col-sm-6">
    {!! Form::label('open', 'Open:') !!}
    {!! Form::text('open', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Close Field -->
<div class="form-group col-sm-6">
    {!! Form::label('close', 'Close:') !!}
    {!! Form::text('close', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Dealer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    {!! Form::number('dealer_id', null, ['class' => 'form-control', 'required']) !!}
</div>