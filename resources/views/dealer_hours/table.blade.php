<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="dealer-hours-table">
            <thead>
            <tr>
                <th>Day</th>
                <th>Open</th>
                <th>Close</th>
                <th>Dealer Id</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dealerHours as $dealerHour)
                <tr>
                    <td>{{ $dealerHour->day }}</td>
                    <td>{{ $dealerHour->open }}</td>
                    <td>{{ $dealerHour->close }}</td>
                    <td>{{ $dealerHour->dealer_id }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['dealerHours.destroy', $dealerHour->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('dealerHours.show', [$dealerHour->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('dealerHours.edit', [$dealerHour->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $dealerHours])
        </div>
    </div>
</div>
