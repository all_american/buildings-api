<!-- Day Field -->
<div class="col-sm-12">
    {!! Form::label('day', 'Day:') !!}
    <p>{{ $dealerHour->day }}</p>
</div>

<!-- Open Field -->
<div class="col-sm-12">
    {!! Form::label('open', 'Open:') !!}
    <p>{{ $dealerHour->open }}</p>
</div>

<!-- Close Field -->
<div class="col-sm-12">
    {!! Form::label('close', 'Close:') !!}
    <p>{{ $dealerHour->close }}</p>
</div>

<!-- Dealer Id Field -->
<div class="col-sm-12">
    {!! Form::label('dealer_id', 'Dealer Id:') !!}
    <p>{{ $dealerHour->dealer_id }}</p>
</div>

