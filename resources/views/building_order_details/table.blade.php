<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="building-order-details-table">
            <thead>
            <tr>
                <th>Building Order Id</th>
                <th>Item</th>
                <th>Price</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($buildingOrderDetails as $buildingOrderDetail)
                <tr>
                    <td>{{ $buildingOrderDetail->building_order_id }}</td>
                    <td>{{ $buildingOrderDetail->item }}</td>
                    <td>{{ $buildingOrderDetail->price }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['buildingOrderDetails.destroy', $buildingOrderDetail->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('buildingOrderDetails.show', [$buildingOrderDetail->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('buildingOrderDetails.edit', [$buildingOrderDetail->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $buildingOrderDetails])
        </div>
    </div>
</div>
