<!-- Building Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_order_id', 'Building Order Id:') !!}
    {!! Form::number('building_order_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Item Field -->
<div class="form-group col-sm-6">
    {!! Form::label('item', 'Item:') !!}
    {!! Form::text('item', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control', 'required']) !!}
</div>