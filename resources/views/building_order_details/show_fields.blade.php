<!-- Building Order Id Field -->
<div class="col-sm-12">
    {!! Form::label('building_order_id', 'Building Order Id:') !!}
    <p>{{ $buildingOrderDetail->building_order_id }}</p>
</div>

<!-- Item Field -->
<div class="col-sm-12">
    {!! Form::label('item', 'Item:') !!}
    <p>{{ $buildingOrderDetail->item }}</p>
</div>

<!-- Price Field -->
<div class="col-sm-12">
    {!! Form::label('price', 'Price:') !!}
    <p>{{ $buildingOrderDetail->price }}</p>
</div>

