<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="manufactures-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($manufactures as $manufacture)
                <tr>
                    <td>{{ $manufacture->name }}</td>
                    <td>{{ $manufacture->status }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['manufactures.destroy', $manufacture->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('manufactures.show', [$manufacture->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('manufactures.edit', [$manufacture->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $manufactures])
        </div>
    </div>
</div>
