<!-- Name Field -->
<div class="col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $dealer->name }}</p>
</div>

<!-- Principal Address Field -->
<div class="col-sm-12">
    {!! Form::label('principal_address', 'Principal Address:') !!}
    <p>{{ $dealer->principal_address }}</p>
</div>

<!-- City Field -->
<div class="col-sm-12">
    {!! Form::label('city', 'City:') !!}
    <p>{{ $dealer->city }}</p>
</div>

<!-- State Id Field -->
<div class="col-sm-12">
    {!! Form::label('state_id', 'State Id:') !!}
    <p>{{ $dealer->state_id }}</p>
</div>

<!-- Zip Code Field -->
<div class="col-sm-12">
    {!! Form::label('zip_code', 'Zip Code:') !!}
    <p>{{ $dealer->zip_code }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $dealer->email }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $dealer->phone }}</p>
</div>

<!-- Website Field -->
<div class="col-sm-12">
    {!! Form::label('website', 'Website:') !!}
    <p>{{ $dealer->website }}</p>
</div>

<!-- Commission Field -->
<div class="col-sm-12">
    {!! Form::label('commission', 'Commission:') !!}
    <p>{{ $dealer->commission }}</p>
</div>

<!-- Owner Name Field -->
<div class="col-sm-12">
    {!! Form::label('owner_name', 'Owner Name:') !!}
    <p>{{ $dealer->owner_name }}</p>
</div>

<!-- Fax Number Field -->
<div class="col-sm-12">
    {!! Form::label('fax_number', 'Fax Number:') !!}
    <p>{{ $dealer->fax_number }}</p>
</div>

<!-- Coverage Miles Field -->
<div class="col-sm-12">
    {!! Form::label('coverage_miles', 'Coverage Miles:') !!}
    <p>{{ $dealer->coverage_miles }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $dealer->type }}</p>
</div>

<!-- Years In Business Field -->
<div class="col-sm-12">
    {!! Form::label('years_in_business', 'Years In Business:') !!}
    <p>{{ $dealer->years_in_business }}</p>
</div>

<!-- Number Of Locations Field -->
<div class="col-sm-12">
    {!! Form::label('number_of_locations', 'Number Of Locations:') !!}
    <p>{{ $dealer->number_of_locations }}</p>
</div>

<!-- Ein Number Field -->
<div class="col-sm-12">
    {!! Form::label('ein_number', 'Ein Number:') !!}
    <p>{{ $dealer->ein_number }}</p>
</div>

<!-- Ein File Field -->
<div class="col-sm-12">
    {!! Form::label('ein_file', 'Ein File:') !!}
    <p>{{ $dealer->ein_file }}</p>
</div>

<!-- Logo Field -->
<div class="col-sm-12">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ $dealer->logo }}</p>
</div>

<!-- Latitude Field -->
<div class="col-sm-12">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{{ $dealer->latitude }}</p>
</div>

<!-- Longitude Field -->
<div class="col-sm-12">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{{ $dealer->longitude }}</p>
</div>

