<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Principal Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('principal_address', 'Principal Address:') !!}
    {!! Form::text('principal_address', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- City Field -->
<div class="form-group col-sm-6">
    {!! Form::label('city', 'City:') !!}
    {!! Form::text('city', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- State Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state_id', 'State Id:') !!}
    {!! Form::number('state_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Zip Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('zip_code', 'Zip Code:') !!}
    {!! Form::text('zip_code', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Website Field -->
<div class="form-group col-sm-6">
    {!! Form::label('website', 'Website:') !!}
    {!! Form::text('website', null, ['class' => 'form-control']) !!}
</div>

<!-- Commission Field -->
<div class="form-group col-sm-6">
    {!! Form::label('commission', 'Commission:') !!}
    {!! Form::number('commission', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Owner Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('owner_name', 'Owner Name:') !!}
    {!! Form::text('owner_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Fax Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fax_number', 'Fax Number:') !!}
    {!! Form::text('fax_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Coverage Miles Field -->
<div class="form-group col-sm-6">
    {!! Form::label('coverage_miles', 'Coverage Miles:') !!}
    {!! Form::number('coverage_miles', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Years In Business Field -->
<div class="form-group col-sm-6">
    {!! Form::label('years_in_business', 'Years In Business:') !!}
    {!! Form::number('years_in_business', null, ['class' => 'form-control']) !!}
</div>

<!-- Number Of Locations Field -->
<div class="form-group col-sm-6">
    {!! Form::label('number_of_locations', 'Number Of Locations:') !!}
    {!! Form::number('number_of_locations', null, ['class' => 'form-control']) !!}
</div>

<!-- Ein Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ein_number', 'Ein Number:') !!}
    {!! Form::text('ein_number', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Ein File Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ein_file', 'Ein File:') !!}
    {!! Form::text('ein_file', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::text('logo', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
</div>