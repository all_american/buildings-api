<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="dealers-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Principal Address</th>
                <th>City</th>
                <th>State Id</th>
                <th>Zip Code</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Website</th>
                <th>Commission</th>
                <th>Owner Name</th>
                <th>Fax Number</th>
                <th>Coverage Miles</th>
                <th>Type</th>
                <th>Years In Business</th>
                <th>Number Of Locations</th>
                <th>Ein Number</th>
                <th>Ein File</th>
                <th>Logo</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($dealers as $dealer)
                <tr>
                    <td>{{ $dealer->name }}</td>
                    <td>{{ $dealer->principal_address }}</td>
                    <td>{{ $dealer->city }}</td>
                    <td>{{ $dealer->state_id }}</td>
                    <td>{{ $dealer->zip_code }}</td>
                    <td>{{ $dealer->email }}</td>
                    <td>{{ $dealer->phone }}</td>
                    <td>{{ $dealer->website }}</td>
                    <td>{{ $dealer->commission }}</td>
                    <td>{{ $dealer->owner_name }}</td>
                    <td>{{ $dealer->fax_number }}</td>
                    <td>{{ $dealer->coverage_miles }}</td>
                    <td>{{ $dealer->type }}</td>
                    <td>{{ $dealer->years_in_business }}</td>
                    <td>{{ $dealer->number_of_locations }}</td>
                    <td>{{ $dealer->ein_number }}</td>
                    <td>{{ $dealer->ein_file }}</td>
                    <td>{{ $dealer->logo }}</td>
                    <td>{{ $dealer->latitude }}</td>
                    <td>{{ $dealer->longitude }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['dealers.destroy', $dealer->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('dealers.show', [$dealer->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('dealers.edit', [$dealer->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $dealers])
        </div>
    </div>
</div>
