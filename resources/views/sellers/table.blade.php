<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="sellers-table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sellers as $seller)
                <tr>
                    <td>{{ $seller->name }}</td>
                    <td>{{ $seller->status }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['sellers.destroy', $seller->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('sellers.show', [$seller->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('sellers.edit', [$seller->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $sellers])
        </div>
    </div>
</div>
