<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="repairs-table">
            <thead>
            <tr>
                <th>User Id</th>
                <th>Building Order Id</th>
                <th>Customer Statement</th>
                <th>Dealer Statement</th>
                <th>Contractor Statement</th>
                <th>Repair Details</th>
                <th>Total</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($repairs as $repair)
                <tr>
                    <td>{{ $repair->user_id }}</td>
                    <td>{{ $repair->building_order_id }}</td>
                    <td>{{ $repair->customer_statement }}</td>
                    <td>{{ $repair->dealer_statement }}</td>
                    <td>{{ $repair->contractor_statement }}</td>
                    <td>{{ $repair->repair_details }}</td>
                    <td>{{ $repair->total }}</td>
                    <td  style="width: 120px">
                        {!! Form::open(['route' => ['repairs.destroy', $repair->id], 'method' => 'delete']) !!}
                        <div class='btn-group'>
                            <a href="{{ route('repairs.show', [$repair->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('repairs.edit', [$repair->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                        </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            @include('adminlte-templates::common.paginate', ['records' => $repairs])
        </div>
    </div>
</div>
