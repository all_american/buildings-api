<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $repair->user_id }}</p>
</div>

<!-- Building Order Id Field -->
<div class="col-sm-12">
    {!! Form::label('building_order_id', 'Building Order Id:') !!}
    <p>{{ $repair->building_order_id }}</p>
</div>

<!-- Customer Statement Field -->
<div class="col-sm-12">
    {!! Form::label('customer_statement', 'Customer Statement:') !!}
    <p>{{ $repair->customer_statement }}</p>
</div>

<!-- Dealer Statement Field -->
<div class="col-sm-12">
    {!! Form::label('dealer_statement', 'Dealer Statement:') !!}
    <p>{{ $repair->dealer_statement }}</p>
</div>

<!-- Contractor Statement Field -->
<div class="col-sm-12">
    {!! Form::label('contractor_statement', 'Contractor Statement:') !!}
    <p>{{ $repair->contractor_statement }}</p>
</div>

<!-- Repair Details Field -->
<div class="col-sm-12">
    {!! Form::label('repair_details', 'Repair Details:') !!}
    <p>{{ $repair->repair_details }}</p>
</div>

<!-- Total Field -->
<div class="col-sm-12">
    {!! Form::label('total', 'Total:') !!}
    <p>{{ $repair->total }}</p>
</div>

