<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Building Order Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('building_order_id', 'Building Order Id:') !!}
    {!! Form::number('building_order_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Customer Statement Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_statement', 'Customer Statement:') !!}
    {!! Form::text('customer_statement', null, ['class' => 'form-control']) !!}
</div>

<!-- Dealer Statement Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dealer_statement', 'Dealer Statement:') !!}
    {!! Form::text('dealer_statement', null, ['class' => 'form-control']) !!}
</div>

<!-- Contractor Statement Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contractor_statement', 'Contractor Statement:') !!}
    {!! Form::text('contractor_statement', null, ['class' => 'form-control']) !!}
</div>

<!-- Repair Details Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repair_details', 'Repair Details:') !!}
    {!! Form::text('repair_details', null, ['class' => 'form-control']) !!}
</div>

<!-- Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('total', 'Total:') !!}
    {!! Form::number('total', null, ['class' => 'form-control', 'required']) !!}
</div>