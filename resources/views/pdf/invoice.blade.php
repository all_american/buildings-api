<div style="display: flex; justify-content: center; flex-direction: column; min-width:500px; margin: 15px 33px;">
    <div style="min-width:400px; display: flex; align-items: center; font-size: 8px;">
        <div class="logo" style="width: 30%; margin-right: 5%;">
            <img width="80%" src="./assets/images/fc.png" alt="" style="margin-left: -6px;" *ngIf="(tagUser === 'fc')?true : false">
        </div>
        <div class="address" style="text-align: center; margin-right: 20%;">
            <p>
                <strong>Invoice: </strong>
            </p>
            <p>
                <strong>Order Date: {{today}}</strong>
            </p>
            <p>
                <strong>Installed Date: </strong>
            </p>
            <p>
                {{dataUS.company_dir}} - {{dataUS.company_tel}}
            </p>
        </div>
        <div class="current-date" style="text-align: center;">
            <p>Deposit Valid</p>
            <p>For 1 Year!</p>
        </div>
    </div>
    <div style=" min-width:500px; font-size: 8px; margin-top: 5px;">
        <div style="font-size:12px; margin-bottom: 5px;">
            <p>Order Status: <strong>{{findNameStatusById(invoice.status) }}</strong></p>
        </div>
        <div style="display: flex;  width: 100%; margin-bottom: 5px;">
            <div style="width: 60%; display: flex; ">
                Dealer: <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;">{{dealer.name}}</div>
            </div>
            <div style="width: 40%;  display: flex; margin-left:10px ;">
                Phone: <div style="width: 85%; border: .3px solid #ccc; margin-left: 5px;" >{{dealer.phone}}</div>
            </div>
        </div>
        <div style="display: flex;  width: 100%; margin-top: 3px; margin-bottom: 5px;">
            <div style="width: 60%; display: flex; ">
                Buyer: <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;">{{invoice.name}}</div>
            </div>
            <div style="width: 40%;  display: flex; ">
                Email: <div style="width: 86%; border: .3px solid #ccc; margin-left: 5px;" >{{invoice.email}}</div>
            </div>
        </div>

        <div style="display: flex;  width: 100%; margin-top: 3px; margin-bottom: 5px;">
            <div style="width: 50%; display: flex; ">
                <span>Install address:</span> <div style="width: 70%; border: .3px solid #ccc; margin-left: 5px;">{{invoice.address}}</div>
            </div>
            <div style="width: 20%;  display: flex; ">
                City: <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;" >{{invoice.city}}</div>
            </div>
            <div style="width: 10%;  display: flex;  margin-left: 10px;">
                State: <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;" >{{invoice.state_id}}</div>
            </div>
            <div style="width: 20%;  display: flex; margin-left: 10px;">
                Zip: <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;" >{{invoice.zip}}</div>
            </div>
        </div>

        <div style="display: flex;  width: 100%; margin-top: 3px; margin-bottom: 5px;">
            <div style="width: 40%; display: flex; ">
                <span>Cell:</span> <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;">{{invoice.phone}}</div>
            </div>
            <div style="width: 30%;  display: flex; ">
                County: <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;" >{{invoice.phone2}}</div>
            </div>
            <div style="width: 30%;  display: flex; margin-left: 10px;">
                Phone: <div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;" >{{invoice.phone3}}</div>
            </div>
        </div>

        <div style="display: flex;  width: 100%; margin-top: 3px;">
            <div style="width: 20%; display: flex;">
                Width:<div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;">{{invoice.width}}</div>
            </div>
            <div style="width: 20%; display: flex; margin-left: 10px;">
                Roof:<div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;">{{invoice.roof_length}}</div>
            </div>
            <div style="width: 20%; display: flex; margin-left: 10px;">
                Frame:<div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;">{{invoice.frame_length}}</div>
            </div>
            <div style="width: 20%; display: flex; margin-left: 10px;">
                Height:<div style="width: 80%; border: .3px solid #ccc; margin-left: 5px;">{{invoice.leg_height}}</div>
            </div>

        </div>
        <br>
        <div style="display: flex; justify-content: space-between;">
            <table style="width: 65%; margin-right: 10px;">
                <tr style="background-color: #ccc;">
                    <td style="width: 80%; border: .2px solid #ccc;">
                        DESCRIPTION
                    </td>
                    <td style="width: 20%; border: .2px solid #ccc;">
                        PRICE
                    </td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[0].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[0].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[1].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[1].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[2].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[2].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[3].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[3].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[4].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[4].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[5].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[5].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[6].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[6].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[7].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[7].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[8].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[8].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[9].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[9].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[10].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[10].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">{{invoice.data[11].item}}</td>
                    <td style="border: .2px solid #ccc; text-align: center;">{{invoice.data[11].price}}</td>
                </tr>
                <tr>
                    <td style="border: .2px solid #ccc;">
                        <p>Non Tax Fees:</p>
                    </td>
                    <td style="border: .2px solid #ccc; text-align: center;">
                        {{invoice.non_tax_fees}}
                    </td>
                </tr>
            </table>
            <table style="width: 30%; border: 1px solid #ccc;;">
                <tr style="border-bottom: 1px solid #ccc; background-color: #ccc;">
                    <td colspan="2"><span>ALL  ORDERS</span> <span style="margin-left: 10px;">ARE</span> <span style="margin-left: 10px;">P.O.D.</span></td>
                </tr>
                <tr>
                    <td style="width: 50%;">
                        Total Sale :
                    </td>
                    <td style="border: 1px solid #ccc; width: 50%;">
                        {{invoice.total_sales}}
                    </td>
                </tr>
                <!-- <tr>
                  <td style="width: 50%;">Manufactory</td>
                  <td style="border: 1px solid #ccc; width: 50%;"></td>
                </tr> -->
                <tr>
                    <td style="width: 50%;">Tax :</td>
                    <td style="border: 1px solid #ccc; width: 50%;">
                        {{invoice.tax}}
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">Tax Exempt# :</td>
                    <td style="border: 1px solid #ccc; width: 50%;">
                        {{invoice.tax_exempt}}
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">Non Tax Fees :</td>
                    <td style="border: 1px solid #ccc;width: 50%;">{{invoice.non_tax_fees}}</td>
                </tr>
                <tr>
                    <td style="width: 50%;">Total :</td>
                    <td style="border: 1px solid #ccc; width: 50%;">
                        {{invoice.total}}
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">Deposit :</td>
                    <td style="border: 1px solid #ccc; width: 50%;">
                        {{invoice.deposit}}
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">Balance Due :</td>
                    <td style="border: 1px solid #ccc; width: 50%;">
                        {{invoice.balance_due}}
                    </td>
                </tr>
            </table>
        </div>
        <div style="display: flex; margin-top: 25px; justify-content: space-between;" >
            <table style="width: 60%; text-align: left;">
                <tr>
                    <td colspan="2" style="text-align: left;">
                        Colors:
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%;">Roof:</td>
                    <td style="border: 1px solid #ccc; width: 25%;">
                        {{invoice.color_top}}
                    </td>
                    <td style="width: 25%;">Is your surface level?</td>
                    <td style="border: 1px solid #ccc; width: 25%;">
                        {{invoice.surface_level.toUpperCase()}}
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%;">Sides:</td>
                    <td style="border: 1px solid #ccc; width: 25%;">
                        {{invoice.color_sides}}
                    </td>
                    <td style="width: 25%;">Electricity?</td>
                    <td style="border: 1px solid #ccc; width: 25%;">
                        {{invoice.electricity_available.toUpperCase()}}
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%;">Ends:</td>
                    <td style="border: 1px solid #ccc; width: 25%;">
                        {{invoice.color_ends}}
                    </td>
                    <td style="width: 25%;">Installation Surface</td>
                    <td style="border: 1px solid #ccc; width: 25%;">
                        {{invoice.installation_surface.toUpperCase()}}
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%;">Trim:</td>
                    <td style="border: 1px solid #ccc; width: 25%;">
                        {{invoice.color_trim}}
                    </td>
                    <td style="width: 25%;">Payment Method:</td>
                    <td style="border: 1px solid #ccc; width: 25%;">{{invoice.payment.toUpperCase()}}</td>
                </tr>
            </table>
            <div style="width: 30%  ; margin-top: 18px; margin-left: -30%;">
                <p style="border: 1px solid #ccc; text-align: center; background-color:#ccc;">Special Instructions</p>
                <p style="width: 100%; height: 50px; border: 1px solid #ccc;">
                    {{invoice.special_instructions}}
                </p>
            </div>
        </div>
    </div>

    <div style="min-width:500px; margin-top: 5%; font-size: 6px;">
        <div class="row">
            <div class="col-12" style="text-align: justify;">
                <p><strong>Things You Should Know....</strong></p>
                <p>
                    {{dataUS.company_full_name}}. Is not responsible for permits, covenant searches or restrictions. Please contact your local Building Inspector or Homeowners Association for information and notify our company of any requirements for which we must comply. The installation site must be level to qualify for our free installation, if it is not, it is the buyer's responsibility to cover any additional labor and/or provide the materials necessary to make it so. Prior to installation, please have any underground cables, gas lines, or any other utility lines located and marked.  {{dataUS.company_full_name}} will not be responsible for any damage to underground utility lines. NO MORE   than the required deposit is to be paid to the dealer! {{dataUS.company_full_name}} will not be responsible for any monies paid to the dealer above the required deposit amount and such payments will delay the process of your order until those funds have been paid in-full to {{dataUS.company_full_name}} by the dealer. Paying in full to the dealer will not speed up your installation date. ALL DEPOSITS  ARE NON-REFUNDABLE. Please be advised that installation times are subject to change due to contractor availability and weather conditions. IF the contractor is unable to complete your job due to site complications, or inaccurate measurements a $100 return fee (non-tax) will apply. Any returned checks or refunds for over-payments made by credit card will incur a 5% fee.
                </p>
                <br>
                <p>
                    Buyer agrees that the balance shall be due and payable in full at the time of installation. If balances due and owing at the time of installation are not paid in full, buyer shall be in default under this agreement and subject to a $50 late fee. At its sole discretion {{dataUS.company_full_name}} may assess   interest at a rate of 18%   per annum on any unpaid balance. In the event of any unpaid balance buyer agrees to allow {{dataUS.company_full_name}} access to the property to repossess the building. Buyer agrees that in the event of any default under this agreement, buyer shall be responsible for reasonable collection agency costs, any attorney's fees and cost incurred because of the default. JURISDICTION, it is expressly agreed that in any dispute, suit, claim or legal proceeding of any nature arising from the obligations under this agreement, shall be filed in a court of competent jurisdiction. {{dataUS.company_full_name}} reserves the right to terminate this agreement at any time. {{dataUS.company_full_name}} must approve all pricing before contract is valid.
                </p>
            </div>
            <br>
            <div class="col-12" style="text-align: center;">
                <p><strong>I have read and completely understand the above information and give my approval   for construction of the above.</strong></p>
            </div>
            <div class="col-12" style="margin-top: 3%; font-size: 8px;">
                <div style="display: flex; justify-content: space-between;">
                    <span>Buyer Signature:_____________________________</span>
                    <span style="margin-left: -10%;">Date:_______________________</span>
                </div>
            </div>
            <div class="col-12" style="margin-top: 3%; font-size: 8px;">
                <div style="display: flex; justify-content: space-between;">
                    <span>Contractor Name:_____________________________</span>
                    <span style="margin-left: -10%;">Date:_______________________</span>
                </div>
            </div>
        </div>
    </div>
</div>
