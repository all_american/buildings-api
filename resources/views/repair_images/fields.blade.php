<!-- Repair Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('repair_id', 'Repair Id:') !!}
    {!! Form::number('repair_id', null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::text('image', null, ['class' => 'form-control', 'required']) !!}
</div>