<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return response()->json([
        'message' => 'Welcome to the API',
    ]);
});*/

Route::resource('profiles', App\Http\Controllers\ProfileController::class);
Route::resource('dealers', App\Http\Controllers\DealerController::class);
Route::resource('building-orders', App\Http\Controllers\BuildingOrderController::class);
Route::resource('building-order-details', App\Http\Controllers\BuildingOrderDetailController::class);
Route::resource('registers', App\Http\Controllers\RegisterController::class);
Route::resource('statuses', App\Http\Controllers\StatusController::class);
Route::resource('sellers', App\Http\Controllers\SellerController::class);
Route::resource('manufactures', App\Http\Controllers\ManufactureController::class);
Route::resource('report-emails', App\Http\Controllers\ReportEmailController::class);
Route::resource('reports', App\Http\Controllers\ReportController::class);