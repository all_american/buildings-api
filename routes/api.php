<?php

// use Illuminate\Http\Request;
use App\Http\Requests\API\CreateUserAPIRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function () {
    Route::resource('companies', App\Http\Controllers\API\CompanyAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('users', App\Http\Controllers\API\UserAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('profiles', App\Http\Controllers\API\ProfileAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('notifications', App\Http\Controllers\API\NotificationAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('states', App\Http\Controllers\API\StateAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('dealers', App\Http\Controllers\API\DealerAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('dealer-addresses', App\Http\Controllers\API\DealerAddressAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('dealer-forms', App\Http\Controllers\API\DealerFormAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('dealer-images', App\Http\Controllers\API\DealerImageAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('dealer-hours', App\Http\Controllers\API\DealerHourAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('building-orders', App\Http\Controllers\API\BuildingOrderAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('building-order-notes', App\Http\Controllers\API\BuildingOrderNoteAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('building-order-details', App\Http\Controllers\API\BuildingOrderDetailAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('statuses', App\Http\Controllers\API\StatusAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('building-order-statuses', App\Http\Controllers\API\BuildingOrderStatusAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('repairs', App\Http\Controllers\API\RepairAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('repair-images', App\Http\Controllers\API\RepairImageAPIController::class)
        ->except(['create', 'edit']);

    Route::get('building-orders/status/{status}', [App\Http\Controllers\API\BuildingOrderAPIController::class, 'getBuildingOrdersByStatus']);


    Route::resource('registers', App\Http\Controllers\API\RegisterAPIController::class)
        ->except(['create', 'edit']);
    Route::get('building-orders-map', [App\Http\Controllers\API\BuildingOrderAPIController::class, 'getBuildingOrdersMap']);

    Route::post('building-orders-print', [App\Http\Controllers\API\BuildingOrderAPIController::class, 'printBuildingOrder']);

    Route::post('save-register', [App\Http\Controllers\API\RegisterAPIController::class, 'saveRegister']);

    Route::resource('sellers', App\Http\Controllers\API\SellerAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('manufactures', App\Http\Controllers\API\ManufactureAPIController::class)
        ->except(['create', 'edit']);

    Route::get('registers-status/{id}', [App\Http\Controllers\API\RegisterAPIController::class, 'updateStatus']);

    Route::get('registers-report', [App\Http\Controllers\API\RegisterAPIController::class, 'getReportList']);

    Route::post('send-email', [App\Http\Controllers\API\RegisterAPIController::class, 'sendEmail']);

    Route::resource('report-emails', App\Http\Controllers\API\ReportEmailAPIController::class)
        ->except(['create', 'edit']);

    Route::resource('reports', App\Http\Controllers\API\ReportAPIController::class)
        ->except(['create', 'edit']);
});
