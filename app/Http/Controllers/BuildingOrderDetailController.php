<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBuildingOrderDetailRequest;
use App\Http\Requests\UpdateBuildingOrderDetailRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BuildingOrderDetailRepository;
use Illuminate\Http\Request;
use Flash;

class BuildingOrderDetailController extends AppBaseController
{
    /** @var BuildingOrderDetailRepository $buildingOrderDetailRepository*/
    private $buildingOrderDetailRepository;

    public function __construct(BuildingOrderDetailRepository $buildingOrderDetailRepo)
    {
        $this->buildingOrderDetailRepository = $buildingOrderDetailRepo;
    }

    /**
     * Display a listing of the BuildingOrderDetail.
     */
    public function index(Request $request)
    {
        $buildingOrderDetails = $this->buildingOrderDetailRepository->paginate(10);

        return view('building_order_details.index')
            ->with('buildingOrderDetails', $buildingOrderDetails);
    }

    /**
     * Show the form for creating a new BuildingOrderDetail.
     */
    public function create()
    {
        return view('building_order_details.create');
    }

    /**
     * Store a newly created BuildingOrderDetail in storage.
     */
    public function store(CreateBuildingOrderDetailRequest $request)
    {
        $input = $request->all();

        $buildingOrderDetail = $this->buildingOrderDetailRepository->create($input);

        Flash::success('Building Order Detail saved successfully.');

        return redirect(route('buildingOrderDetails.index'));
    }

    /**
     * Display the specified BuildingOrderDetail.
     */
    public function show($id)
    {
        $buildingOrderDetail = $this->buildingOrderDetailRepository->find($id);

        if (empty($buildingOrderDetail)) {
            Flash::error('Building Order Detail not found');

            return redirect(route('buildingOrderDetails.index'));
        }

        return view('building_order_details.show')->with('buildingOrderDetail', $buildingOrderDetail);
    }

    /**
     * Show the form for editing the specified BuildingOrderDetail.
     */
    public function edit($id)
    {
        $buildingOrderDetail = $this->buildingOrderDetailRepository->find($id);

        if (empty($buildingOrderDetail)) {
            Flash::error('Building Order Detail not found');

            return redirect(route('buildingOrderDetails.index'));
        }

        return view('building_order_details.edit')->with('buildingOrderDetail', $buildingOrderDetail);
    }

    /**
     * Update the specified BuildingOrderDetail in storage.
     */
    public function update($id, UpdateBuildingOrderDetailRequest $request)
    {
        $buildingOrderDetail = $this->buildingOrderDetailRepository->find($id);

        if (empty($buildingOrderDetail)) {
            Flash::error('Building Order Detail not found');

            return redirect(route('buildingOrderDetails.index'));
        }

        $buildingOrderDetail = $this->buildingOrderDetailRepository->update($request->all(), $id);

        Flash::success('Building Order Detail updated successfully.');

        return redirect(route('buildingOrderDetails.index'));
    }

    /**
     * Remove the specified BuildingOrderDetail from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $buildingOrderDetail = $this->buildingOrderDetailRepository->find($id);

        if (empty($buildingOrderDetail)) {
            Flash::error('Building Order Detail not found');

            return redirect(route('buildingOrderDetails.index'));
        }

        $this->buildingOrderDetailRepository->delete($id);

        Flash::success('Building Order Detail deleted successfully.');

        return redirect(route('buildingOrderDetails.index'));
    }
}
