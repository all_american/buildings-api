<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSellerAPIRequest;
use App\Http\Requests\API\UpdateSellerAPIRequest;
use App\Models\Seller;
use App\Repositories\SellerRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class SellerAPIController
 */
class SellerAPIController extends AppBaseController
{
    private SellerRepository $sellerRepository;

    public function __construct(SellerRepository $sellerRepo)
    {
        $this->sellerRepository = $sellerRepo;
    }

    /**
     * Display a listing of the Sellers.
     * GET|HEAD /sellers
     */
    public function index(Request $request): JsonResponse
    {
        $sellers = $this->sellerRepository->getAll();

        return $this->sendResponse($sellers, 'Sellers retrieved successfully');
    }

    /**
     * Store a newly created Seller in storage.
     * POST /sellers
     */
    public function store(CreateSellerAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $seller = $this->sellerRepository->create($input);

        return $this->sendResponse($seller->toArray(), 'Seller saved successfully');
    }

    /**
     * Display the specified Seller.
     * GET|HEAD /sellers/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var Seller $seller */
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            return $this->sendError('Seller not found');
        }

        return $this->sendResponse($seller->toArray(), 'Seller retrieved successfully');
    }

    /**
     * Update the specified Seller in storage.
     * PUT/PATCH /sellers/{id}
     */
    public function update($id, UpdateSellerAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Seller $seller */
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            return $this->sendError('Seller not found');
        }

        $seller = $this->sellerRepository->update($input, $id);

        return $this->sendResponse($seller->toArray(), 'Seller updated successfully');
    }

    /**
     * Remove the specified Seller from storage.
     * DELETE /sellers/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var Seller $seller */
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            return $this->sendError('Seller not found');
        }

        $this->sellerRepository->delete($id);

        return $this->sendSuccess('Seller deleted successfully');
    }
}
