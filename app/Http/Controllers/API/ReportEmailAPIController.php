<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReportEmailAPIRequest;
use App\Http\Requests\API\UpdateReportEmailAPIRequest;
use App\Models\ReportEmail;
use App\Repositories\ReportEmailRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class ReportEmailAPIController
 */
class ReportEmailAPIController extends AppBaseController
{
    private ReportEmailRepository $reportEmailRepository;

    public function __construct(ReportEmailRepository $reportEmailRepo)
    {
        $this->reportEmailRepository = $reportEmailRepo;
    }

    /**
     * Display a listing of the ReportEmails.
     * GET|HEAD /report-emails
     */
    public function index(Request $request): JsonResponse
    {
        $reportEmails = $this->reportEmailRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($reportEmails->toArray(), 'Report Emails retrieved successfully');
    }

    /**
     * Store a newly created ReportEmail in storage.
     * POST /report-emails
     */
    public function store(CreateReportEmailAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $reportEmail = $this->reportEmailRepository->create($input);

        return $this->sendResponse($reportEmail->toArray(), 'Report Email saved successfully');
    }

    /**
     * Display the specified ReportEmail.
     * GET|HEAD /report-emails/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var ReportEmail $reportEmail */
        $reportEmail = $this->reportEmailRepository->find($id);

        if (empty($reportEmail)) {
            return $this->sendError('Report Email not found');
        }

        return $this->sendResponse($reportEmail->toArray(), 'Report Email retrieved successfully');
    }

    /**
     * Update the specified ReportEmail in storage.
     * PUT/PATCH /report-emails/{id}
     */
    public function update($id, UpdateReportEmailAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var ReportEmail $reportEmail */
        $reportEmail = $this->reportEmailRepository->find($id);

        if (empty($reportEmail)) {
            return $this->sendError('Report Email not found');
        }

        $reportEmail = $this->reportEmailRepository->update($input, $id);

        return $this->sendResponse($reportEmail->toArray(), 'ReportEmail updated successfully');
    }

    /**
     * Remove the specified ReportEmail from storage.
     * DELETE /report-emails/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var ReportEmail $reportEmail */
        $reportEmail = $this->reportEmailRepository->find($id);

        if (empty($reportEmail)) {
            return $this->sendError('Report Email not found');
        }

        $reportEmail->delete();

        return $this->sendSuccess('Report Email deleted successfully');
    }
}
