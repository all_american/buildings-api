<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRegisterAPIRequest;
use App\Http\Requests\API\UpdateRegisterAPIRequest;
use App\Mail\Quote;
use App\Models\Register;
use App\Repositories\RegisterRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Mail;

/**
 * Class RegisterAPIController
 */
class RegisterAPIController extends AppBaseController
{
    private RegisterRepository $registerRepository;

    public function __construct(RegisterRepository $registerRepo)
    {
        $this->registerRepository = $registerRepo;
    }

    /**
     * Display a listing of the Registers.
     * GET|HEAD /registers
     */
    public function index(Request $request): JsonResponse
    {
        $registers = $this->registerRepository->getAllregisters();

        return $this->sendResponse($registers, 'Registers retrieved successfully');
    }

    /**
     * Store a newly created Register in storage.
     * POST /registers
     */
    public function saveRegister(Request $request): JsonResponse
    {
        $input = $request->all();
        $response = $this->registerRepository->setFileOrder($input,$request);
        if($response['document_path'] === 'null'){
            return $this->sendError('Error with address');
        }
        $register = $this->registerRepository->create($response);

        return $this->sendResponse($register->toArray(), 'Register saved successfully')
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
            ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
    }

    /**
     * Display the specified Register.
     * GET|HEAD /registers/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var Register $register */
        $register = $this->registerRepository->find($id);

        if (empty($register)) {
            return $this->sendError('Register not found');
        }

        return $this->sendResponse($register->toArray(), 'Register retrieved successfully');
    }

    /**
     * Update the specified Register in storage.
     * PUT/PATCH /registers/{id}
     */
    public function update($id, UpdateRegisterAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Register $register */
        $register = $this->registerRepository->find($id);

        if (empty($register)) {
            return $this->sendError('Register not found');
        }

        $register = $this->registerRepository->update($input, $id);

        return $this->sendResponse($register->toArray(), 'Register updated successfully');
    }

    /**
     * Remove the specified Register from storage.
     * DELETE /registers/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var Register $register */
        $register = $this->registerRepository->find($id);

        if (empty($register)) {
            return $this->sendError('Register not found');
        }

        $register->delete();

        return $this->sendSuccess('Register deleted successfully');
    }

    public function updateStatus($id): JsonResponse
    {
        $registers = $this->registerRepository->updateStatus($id);

        return $this->sendResponse($registers, 'Registers retrieved successfully');
    }

    public function getReportList(): JsonResponse
    {
        $registers = $this->registerRepository->getManufatureList();
        return $this->sendResponse($registers, 'Registers retrieved successfully');
    }

    public function sendEmail(Request $request): JsonResponse{
        $data = $request->all();
        $mailData = [
            'title' => $data['title'],
            'body' => $data['body'],
        ];
        Mail::to('info@allamericanbuildings.com')->send(new Quote($mailData));
        return $this->sendResponse($mailData, 'Email Send successfully');
    }
}
