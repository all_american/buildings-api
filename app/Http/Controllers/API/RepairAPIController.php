<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRepairAPIRequest;
use App\Http\Requests\API\UpdateRepairAPIRequest;
use App\Models\Repair;
use App\Repositories\RepairRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class RepairAPIController
 */
class RepairAPIController extends AppBaseController
{
    private RepairRepository $repairRepository;

    public function __construct(RepairRepository $repairRepo)
    {
        $this->repairRepository = $repairRepo;
    }

    /**
     * Display a listing of the Repairs.
     * GET|HEAD /repairs
     */
    public function index(Request $request): JsonResponse
    {
        $repairs = $this->repairRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($repairs->toArray(), 'Repairs retrieved successfully');
    }

    /**
     * Store a newly created Repair in storage.
     * POST /repairs
     */
    public function store(CreateRepairAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $repair = $this->repairRepository->create($input);

        return $this->sendResponse($repair->toArray(), 'Repair saved successfully');
    }

    /**
     * Display the specified Repair.
     * GET|HEAD /repairs/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var Repair $repair */
        $repair = $this->repairRepository->find($id);

        if (empty($repair)) {
            return $this->sendError('Repair not found');
        }

        return $this->sendResponse($repair->toArray(), 'Repair retrieved successfully');
    }

    /**
     * Update the specified Repair in storage.
     * PUT/PATCH /repairs/{id}
     */
    public function update($id, UpdateRepairAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Repair $repair */
        $repair = $this->repairRepository->find($id);

        if (empty($repair)) {
            return $this->sendError('Repair not found');
        }

        $repair = $this->repairRepository->update($input, $id);

        return $this->sendResponse($repair->toArray(), 'Repair updated successfully');
    }

    /**
     * Remove the specified Repair from storage.
     * DELETE /repairs/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var Repair $repair */
        $repair = $this->repairRepository->find($id);

        if (empty($repair)) {
            return $this->sendError('Repair not found');
        }

        $repair->delete();

        return $this->sendSuccess('Repair deleted successfully');
    }
}
