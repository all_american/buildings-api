<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealerImageAPIRequest;
use App\Http\Requests\API\UpdateDealerImageAPIRequest;
use App\Models\DealerImage;
use App\Repositories\DealerImageRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class DealerImageAPIController
 */
class DealerImageAPIController extends AppBaseController
{
    private DealerImageRepository $dealerImageRepository;

    public function __construct(DealerImageRepository $dealerImageRepo)
    {
        $this->dealerImageRepository = $dealerImageRepo;
    }

    /**
     * Display a listing of the DealerImages.
     * GET|HEAD /dealer-images
     */
    public function index(Request $request): JsonResponse
    {
        $dealerImages = $this->dealerImageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dealerImages->toArray(), 'Dealer Images retrieved successfully');
    }

    /**
     * Store a newly created DealerImage in storage.
     * POST /dealer-images
     */
    public function store(CreateDealerImageAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $dealerImage = $this->dealerImageRepository->create($input);

        return $this->sendResponse($dealerImage->toArray(), 'Dealer Image saved successfully');
    }

    /**
     * Display the specified DealerImage.
     * GET|HEAD /dealer-images/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var DealerImage $dealerImage */
        $dealerImage = $this->dealerImageRepository->find($id);

        if (empty($dealerImage)) {
            return $this->sendError('Dealer Image not found');
        }

        return $this->sendResponse($dealerImage->toArray(), 'Dealer Image retrieved successfully');
    }

    /**
     * Update the specified DealerImage in storage.
     * PUT/PATCH /dealer-images/{id}
     */
    public function update($id, UpdateDealerImageAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var DealerImage $dealerImage */
        $dealerImage = $this->dealerImageRepository->find($id);

        if (empty($dealerImage)) {
            return $this->sendError('Dealer Image not found');
        }

        $dealerImage = $this->dealerImageRepository->update($input, $id);

        return $this->sendResponse($dealerImage->toArray(), 'DealerImage updated successfully');
    }

    /**
     * Remove the specified DealerImage from storage.
     * DELETE /dealer-images/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var DealerImage $dealerImage */
        $dealerImage = $this->dealerImageRepository->find($id);

        if (empty($dealerImage)) {
            return $this->sendError('Dealer Image not found');
        }

        $dealerImage->delete();

        return $this->sendSuccess('Dealer Image deleted successfully');
    }
}
