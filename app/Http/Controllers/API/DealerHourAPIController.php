<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealerHourAPIRequest;
use App\Http\Requests\API\UpdateDealerHourAPIRequest;
use App\Models\DealerHour;
use App\Repositories\DealerHourRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class DealerHourAPIController
 */
class DealerHourAPIController extends AppBaseController
{
    private DealerHourRepository $dealerHourRepository;

    public function __construct(DealerHourRepository $dealerHourRepo)
    {
        $this->dealerHourRepository = $dealerHourRepo;
    }

    /**
     * Display a listing of the DealerHours.
     * GET|HEAD /dealer-hours
     */
    public function index(Request $request): JsonResponse
    {
        $dealerHours = $this->dealerHourRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dealerHours->toArray(), 'Dealer Hours retrieved successfully');
    }

    /**
     * Store a newly created DealerHour in storage.
     * POST /dealer-hours
     */
    public function store(CreateDealerHourAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $dealerHour = $this->dealerHourRepository->create($input);

        return $this->sendResponse($dealerHour->toArray(), 'Dealer Hour saved successfully');
    }

    /**
     * Display the specified DealerHour.
     * GET|HEAD /dealer-hours/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var DealerHour $dealerHour */
        $dealerHour = $this->dealerHourRepository->find($id);

        if (empty($dealerHour)) {
            return $this->sendError('Dealer Hour not found');
        }

        return $this->sendResponse($dealerHour->toArray(), 'Dealer Hour retrieved successfully');
    }

    /**
     * Update the specified DealerHour in storage.
     * PUT/PATCH /dealer-hours/{id}
     */
    public function update($id, UpdateDealerHourAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var DealerHour $dealerHour */
        $dealerHour = $this->dealerHourRepository->find($id);

        if (empty($dealerHour)) {
            return $this->sendError('Dealer Hour not found');
        }

        $dealerHour = $this->dealerHourRepository->update($input, $id);

        return $this->sendResponse($dealerHour->toArray(), 'DealerHour updated successfully');
    }

    /**
     * Remove the specified DealerHour from storage.
     * DELETE /dealer-hours/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var DealerHour $dealerHour */
        $dealerHour = $this->dealerHourRepository->find($id);

        if (empty($dealerHour)) {
            return $this->sendError('Dealer Hour not found');
        }

        $dealerHour->delete();

        return $this->sendSuccess('Dealer Hour deleted successfully');
    }
}
