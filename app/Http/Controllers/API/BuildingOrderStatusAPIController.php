<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBuildingOrderStatusAPIRequest;
use App\Http\Requests\API\UpdateBuildingOrderStatusAPIRequest;
use App\Models\BuildingOrderStatus;
use App\Repositories\BuildingOrderStatusRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class BuildingOrderStatusAPIController
 */
class BuildingOrderStatusAPIController extends AppBaseController
{
    private BuildingOrderStatusRepository $buildingOrderStatusRepository;

    public function __construct(BuildingOrderStatusRepository $buildingOrderStatusRepo)
    {
        $this->buildingOrderStatusRepository = $buildingOrderStatusRepo;
    }

    /**
     * Display a listing of the BuildingOrderStatuses.
     * GET|HEAD /building-order-statuses
     */
    public function index(Request $request): JsonResponse
    {
        $buildingOrderStatuses = $this->buildingOrderStatusRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($buildingOrderStatuses->toArray(), 'Building Order Statuses retrieved successfully');
    }

    /**
     * Store a newly created BuildingOrderStatus in storage.
     * POST /building-order-statuses
     */
    public function store(CreateBuildingOrderStatusAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $buildingOrderStatus = $this->buildingOrderStatusRepository->create($input);

        return $this->sendResponse($buildingOrderStatus->toArray(), 'Building Order Status saved successfully');
    }

    /**
     * Display the specified BuildingOrderStatus.
     * GET|HEAD /building-order-statuses/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var BuildingOrderStatus $buildingOrderStatus */
        $buildingOrderStatus = $this->buildingOrderStatusRepository->find($id);

        if (empty($buildingOrderStatus)) {
            return $this->sendError('Building Order Status not found');
        }

        return $this->sendResponse($buildingOrderStatus->toArray(), 'Building Order Status retrieved successfully');
    }

    /**
     * Update the specified BuildingOrderStatus in storage.
     * PUT/PATCH /building-order-statuses/{id}
     */
    public function update($id, UpdateBuildingOrderStatusAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var BuildingOrderStatus $buildingOrderStatus */
        $buildingOrderStatus = $this->buildingOrderStatusRepository->find($id);

        if (empty($buildingOrderStatus)) {
            return $this->sendError('Building Order Status not found');
        }

        $buildingOrderStatus = $this->buildingOrderStatusRepository->update($input, $id);

        return $this->sendResponse($buildingOrderStatus->toArray(), 'BuildingOrderStatus updated successfully');
    }

    /**
     * Remove the specified BuildingOrderStatus from storage.
     * DELETE /building-order-statuses/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var BuildingOrderStatus $buildingOrderStatus */
        $buildingOrderStatus = $this->buildingOrderStatusRepository->find($id);

        if (empty($buildingOrderStatus)) {
            return $this->sendError('Building Order Status not found');
        }

        $buildingOrderStatus->delete();

        return $this->sendSuccess('Building Order Status deleted successfully');
    }
}
