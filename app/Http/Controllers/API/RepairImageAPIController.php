<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRepairImageAPIRequest;
use App\Http\Requests\API\UpdateRepairImageAPIRequest;
use App\Models\RepairImage;
use App\Repositories\RepairImageRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class RepairImageAPIController
 */
class RepairImageAPIController extends AppBaseController
{
    private RepairImageRepository $repairImageRepository;

    public function __construct(RepairImageRepository $repairImageRepo)
    {
        $this->repairImageRepository = $repairImageRepo;
    }

    /**
     * Display a listing of the RepairImages.
     * GET|HEAD /repair-images
     */
    public function index(Request $request): JsonResponse
    {
        $repairImages = $this->repairImageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($repairImages->toArray(), 'Repair Images retrieved successfully');
    }

    /**
     * Store a newly created RepairImage in storage.
     * POST /repair-images
     */
    public function store(CreateRepairImageAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $repairImage = $this->repairImageRepository->create($input);

        return $this->sendResponse($repairImage->toArray(), 'Repair Image saved successfully');
    }

    /**
     * Display the specified RepairImage.
     * GET|HEAD /repair-images/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var RepairImage $repairImage */
        $repairImage = $this->repairImageRepository->find($id);

        if (empty($repairImage)) {
            return $this->sendError('Repair Image not found');
        }

        return $this->sendResponse($repairImage->toArray(), 'Repair Image retrieved successfully');
    }

    /**
     * Update the specified RepairImage in storage.
     * PUT/PATCH /repair-images/{id}
     */
    public function update($id, UpdateRepairImageAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var RepairImage $repairImage */
        $repairImage = $this->repairImageRepository->find($id);

        if (empty($repairImage)) {
            return $this->sendError('Repair Image not found');
        }

        $repairImage = $this->repairImageRepository->update($input, $id);

        return $this->sendResponse($repairImage->toArray(), 'RepairImage updated successfully');
    }

    /**
     * Remove the specified RepairImage from storage.
     * DELETE /repair-images/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var RepairImage $repairImage */
        $repairImage = $this->repairImageRepository->find($id);

        if (empty($repairImage)) {
            return $this->sendError('Repair Image not found');
        }

        $repairImage->delete();

        return $this->sendSuccess('Repair Image deleted successfully');
    }
}
