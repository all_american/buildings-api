<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBuildingOrderDetailAPIRequest;
use App\Http\Requests\API\UpdateBuildingOrderDetailAPIRequest;
use App\Models\BuildingOrderDetail;
use App\Repositories\BuildingOrderDetailRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class BuildingOrderDetailAPIController
 */
class BuildingOrderDetailAPIController extends AppBaseController
{
    private BuildingOrderDetailRepository $buildingOrderDetailRepository;

    public function __construct(BuildingOrderDetailRepository $buildingOrderDetailRepo)
    {
        $this->buildingOrderDetailRepository = $buildingOrderDetailRepo;
    }

    /**
     * Display a listing of the BuildingOrderDetails.
     * GET|HEAD /building-order-details
     */
    public function index(Request $request): JsonResponse
    {
        $buildingOrderDetails = $this->buildingOrderDetailRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($buildingOrderDetails->toArray(), 'Building Order Details retrieved successfully');
    }

    /**
     * Store a newly created BuildingOrderDetail in storage.
     * POST /building-order-details
     */
    public function store(CreateBuildingOrderDetailAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $buildingOrderDetail = $this->buildingOrderDetailRepository->create($input);

        return $this->sendResponse($buildingOrderDetail->toArray(), 'Building Order Detail saved successfully');
    }

    /**
     * Display the specified BuildingOrderDetail.
     * GET|HEAD /building-order-details/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var BuildingOrderDetail $buildingOrderDetail */
        $buildingOrderDetail = $this->buildingOrderDetailRepository->find($id);

        if (empty($buildingOrderDetail)) {
            return $this->sendError('Building Order Detail not found');
        }

        return $this->sendResponse($buildingOrderDetail->toArray(), 'Building Order Detail retrieved successfully');
    }

    /**
     * Update the specified BuildingOrderDetail in storage.
     * PUT/PATCH /building-order-details/{id}
     */
    public function update($id, UpdateBuildingOrderDetailAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var BuildingOrderDetail $buildingOrderDetail */
        $buildingOrderDetail = $this->buildingOrderDetailRepository->find($id);

        if (empty($buildingOrderDetail)) {
            return $this->sendError('Building Order Detail not found');
        }

        $buildingOrderDetail = $this->buildingOrderDetailRepository->update($input, $id);

        return $this->sendResponse($buildingOrderDetail->toArray(), 'BuildingOrderDetail updated successfully');
    }

    /**
     * Remove the specified BuildingOrderDetail from storage.
     * DELETE /building-order-details/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var BuildingOrderDetail $buildingOrderDetail */
        $buildingOrderDetail = $this->buildingOrderDetailRepository->find($id);

        if (empty($buildingOrderDetail)) {
            return $this->sendError('Building Order Detail not found');
        }

        $buildingOrderDetail->delete();

        return $this->sendSuccess('Building Order Detail deleted successfully');
    }
}
