<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBuildingOrderAPIRequest;
use App\Http\Requests\API\UpdateBuildingOrderAPIRequest;
use App\Models\BuildingOrder;
use App\Repositories\BuildingOrderDetailRepository;
use App\Repositories\BuildingOrderRepository;
use App\Repositories\BuildingOrderStatusRepository;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class BuildingOrderAPIController
 */
class BuildingOrderAPIController extends AppBaseController
{
    private BuildingOrderRepository $buildingOrderRepository;
    private BuildingOrderDetailRepository $buildingOrderDetailRepository;
    private BuildingOrderStatusRepository $buildingOrderStatusRepository;

    public function __construct(BuildingOrderRepository $buildingOrderRepo, BuildingOrderDetailRepository $buildingOrderDetailRepo, BuildingOrderStatusRepository $buildingOrderStatusRepo)
    {
        $this->buildingOrderRepository = $buildingOrderRepo;
        $this->buildingOrderDetailRepository = $buildingOrderDetailRepo;
        $this->buildingOrderStatusRepository = $buildingOrderStatusRepo;
    }

    /**
     * Display a listing of the BuildingOrders.
     * GET|HEAD /building-orders
     */
    public function index(Request $request): JsonResponse
    {
        $buildingOrders = $this->buildingOrderRepository->getOrders();
        return $this->sendResponse($buildingOrders, 'Building Orders retrieved successfully');
    }

    /**
     * Display a listing of the BuildingOrders.
     * GET /building-orders/status/{status}
     */

    public function getBuildingOrdersByStatus(string $status): JsonResponse
    {
        $buildingOrders = $this->buildingOrderRepository->getOrdersByStatus($status);
        return $this->sendResponse($buildingOrders, 'Building Orders retrieved successfully');
    }

    /**
     * Store a newly created BuildingOrder in storage.
     * POST /building-orders
     */
    public function store(Request $request): JsonResponse
    {
        $input = $request->all();
        $order = [
            'user_id' => $input['user_id'],
            'dealer_id' => $input['dealer_id'],
            'invoice_number' => $input['invoice_number'],
            'invoice_date' => $input['invoice_date'],
            'name' => $input['name'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'phone2' => $input['phone2'],
            'phone3' => $input['phone3'],
            'address' => $input['address'],
            'city' => $input['city'],
            'state_id' => $input['state_id'],
            'zip' => $input['zip'],
            'width' => $input['width'],
            'roof_length' => $input['roof_length'],
            'frame_length' => $input['frame_length'],
            'leg_height' => $input['leg_height'],
            'gauge' => $input['gauge'],
            'price' => $input['price'],
            'description' => $input['description'],
            'description_price' => $input['description_price'],
            'color_top' => $input['color_top'],
            'color_sides' => $input['color_sides'],
            'color_ends' => $input['color_ends'],
            'color_trim' => $input['color_trim'],
            'non_tax_fees_desc' => $input['non_tax_fees_desc'],
            'non_tax_fees' => $input['non_tax_fees'],
            'installation_address' => $input['installation_address'],
            'installation_city' => $input['installation_city'],
            'installation_state_id' => $input['installation_state_id'],
            'installation_zip' => $input['installation_zip'],
            'total_tax' => $input['total_tax'],
            'total_sales' => $input['total_sales'],
            'tax' => $input['tax'],
            'tax_exempt' => $input['tax_exempt'],
            'total' => $input['total'],
            'deposit' => $input['deposit'],
            'balance_due' => $input['balance_due'],
            'special_instructions' => $input['special_instructions'],
            'surface_level' => $input['surface_level'],
            'electricity_available' => $input['electricity_available'],
            'installation_surface' => $input['installation_surface'],
            'payment' => $input['payment'],
            'ready_install' => $input['ready_install'],
        ];
        $data = $input['data'];
        $order = $this->buildingOrderRepository->getLatitudeAndLongitude($order);
        $buildingOrder = $this->buildingOrderRepository->create($order);
        foreach ($data as $item) {
            $item['building_order_id'] = $buildingOrder->id;
            $this->buildingOrderDetailRepository->create($item);
        }
        $this->buildingOrderStatusRepository->create([
            'building_order_id' => $buildingOrder->id,
            'status_id' => 1,
            'user_id' => $buildingOrder->user_id,
        ]);
        $last = $this->buildingOrderRepository->getOrder($buildingOrder->id);
        return $this->sendResponse($last->toArray(), 'Building Order saved successfully');
    }

    /**
     * Display the specified BuildingOrder.
     * GET|HEAD /building-orders/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var BuildingOrder $buildingOrder */
        $buildingOrder = $this->buildingOrderRepository->find($id);
        if (empty($buildingOrder)) {
            return $this->sendError('Building Order not found');
        }
        $buildingOrder = $this->buildingOrderRepository->getOrder($id);
        return $this->sendResponse($buildingOrder->toArray(), 'Building Order retrieved successfully');
    }

    /**
     * Update the specified BuildingOrder in storage.
     * PUT/PATCH /building-orders/{id}
     */
    public function update($id, UpdateBuildingOrderAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var BuildingOrder $buildingOrder */
        $buildingOrder = $this->buildingOrderRepository->find($id);

        if (empty($buildingOrder)) {
            return $this->sendError('Building Order not found');
        }

        $buildingOrder = $this->buildingOrderRepository->update($input, $id);

        return $this->sendResponse($buildingOrder->toArray(), 'BuildingOrder updated successfully');
    }

    /**
     * Remove the specified BuildingOrder from storage.
     * DELETE /building-orders/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var BuildingOrder $buildingOrder */
        $buildingOrder = $this->buildingOrderRepository->find($id);

        if (empty($buildingOrder)) {
            return $this->sendError('Building Order not found');
        }

        $buildingOrder->delete();

        return $this->sendSuccess('Building Order deleted successfully');
    }

    /**
     * Display a listing of the BuildingOrders.
     * GET /building-orders/map
     */

    public function getBuildingOrdersMap(): JsonResponse
    {
        $buildingOrders = $this->buildingOrderRepository->getOrdersMap();
        return $this->sendResponse($buildingOrders, 'Building Orders retrieved successfully');
    }

    /**
     * Store a newly created BuildingOrder in storage and print
     * POST /building-orders-print
     */

    /**
     * Display the specified BuildingOrder.
     * GET|HEAD /building-orders-print/{id}
     */

    public function printBuildingOrder(Request $request)
    {
        $input = $request->all();
        $order = [
            'user_id' => $input['user_id'],
            'dealer_id' => $input['dealer_id'],
            'invoice_number' => $input['invoice_number'],
            'invoice_date' => $input['invoice_date'],
            'name' => $input['name'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'phone2' => $input['phone2'],
            'phone3' => $input['phone3'],
            'address' => $input['address'],
            'city' => $input['city'],
            'state_id' => $input['state_id'],
            'zip' => $input['zip'],
            'width' => $input['width'],
            'roof_length' => $input['roof_length'],
            'frame_length' => $input['frame_length'],
            'leg_height' => $input['leg_height'],
            'gauge' => $input['gauge'],
            'price' => $input['price'],
            'description' => $input['description'],
            'description_price' => $input['description_price'],
            'color_top' => $input['color_top'],
            'color_sides' => $input['color_sides'],
            'color_ends' => $input['color_ends'],
            'color_trim' => $input['color_trim'],
            'non_tax_fees_desc' => $input['non_tax_fees_desc'],
            'non_tax_fees' => $input['non_tax_fees'],
            'installation_address' => $input['installation_address'],
            'installation_city' => $input['installation_city'],
            'installation_state_id' => $input['installation_state_id'],
            'installation_zip' => $input['installation_zip'],
            'total_tax' => $input['total_tax'],
            'total_sales' => $input['total_sales'],
            'tax' => $input['tax'],
            'tax_exempt' => $input['tax_exempt'],
            'total' => $input['total'],
            'deposit' => $input['deposit'],
            'balance_due' => $input['balance_due'],
            'special_instructions' => $input['special_instructions'],
            'surface_level' => $input['surface_level'],
            'electricity_available' => $input['electricity_available'],
            'installation_surface' => $input['installation_surface'],
            'payment' => $input['payment'],
            'ready_install' => $input['ready_install'],
        ];
        $data = $input['data'];
        $buildingOrder = [
            'order' => $order,
            'data' => $data
        ];
        $pdf = PDF::loadView('building_orders.print', compact('buildingOrder'));
        return $pdf->stream('building_order.pdf');

    }
}
