<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBuildingOrderNoteAPIRequest;
use App\Http\Requests\API\UpdateBuildingOrderNoteAPIRequest;
use App\Models\BuildingOrderNote;
use App\Repositories\BuildingOrderNoteRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class BuildingOrderNoteAPIController
 */
class BuildingOrderNoteAPIController extends AppBaseController
{
    private BuildingOrderNoteRepository $buildingOrderNoteRepository;

    public function __construct(BuildingOrderNoteRepository $buildingOrderNoteRepo)
    {
        $this->buildingOrderNoteRepository = $buildingOrderNoteRepo;
    }

    /**
     * Display a listing of the BuildingOrderNotes.
     * GET|HEAD /building-order-notes
     */
    public function index(Request $request): JsonResponse
    {
        $buildingOrderNotes = $this->buildingOrderNoteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($buildingOrderNotes->toArray(), 'Building Order Notes retrieved successfully');
    }

    /**
     * Store a newly created BuildingOrderNote in storage.
     * POST /building-order-notes
     */
    public function store(CreateBuildingOrderNoteAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $buildingOrderNote = $this->buildingOrderNoteRepository->create($input);

        return $this->sendResponse($buildingOrderNote->toArray(), 'Building Order Note saved successfully');
    }

    /**
     * Display the specified BuildingOrderNote.
     * GET|HEAD /building-order-notes/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var BuildingOrderNote $buildingOrderNote */
        $buildingOrderNote = $this->buildingOrderNoteRepository->find($id);

        if (empty($buildingOrderNote)) {
            return $this->sendError('Building Order Note not found');
        }

        return $this->sendResponse($buildingOrderNote->toArray(), 'Building Order Note retrieved successfully');
    }

    /**
     * Update the specified BuildingOrderNote in storage.
     * PUT/PATCH /building-order-notes/{id}
     */
    public function update($id, UpdateBuildingOrderNoteAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var BuildingOrderNote $buildingOrderNote */
        $buildingOrderNote = $this->buildingOrderNoteRepository->find($id);

        if (empty($buildingOrderNote)) {
            return $this->sendError('Building Order Note not found');
        }

        $buildingOrderNote = $this->buildingOrderNoteRepository->update($input, $id);

        return $this->sendResponse($buildingOrderNote->toArray(), 'BuildingOrderNote updated successfully');
    }

    /**
     * Remove the specified BuildingOrderNote from storage.
     * DELETE /building-order-notes/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var BuildingOrderNote $buildingOrderNote */
        $buildingOrderNote = $this->buildingOrderNoteRepository->find($id);

        if (empty($buildingOrderNote)) {
            return $this->sendError('Building Order Note not found');
        }

        $buildingOrderNote->delete();

        return $this->sendSuccess('Building Order Note deleted successfully');
    }
}
