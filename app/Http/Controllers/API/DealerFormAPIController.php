<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealerFormAPIRequest;
use App\Http\Requests\API\UpdateDealerFormAPIRequest;
use App\Models\DealerForm;
use App\Repositories\DealerFormRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class DealerFormAPIController
 */
class DealerFormAPIController extends AppBaseController
{
    private DealerFormRepository $dealerFormRepository;

    public function __construct(DealerFormRepository $dealerFormRepo)
    {
        $this->dealerFormRepository = $dealerFormRepo;
    }

    /**
     * Display a listing of the DealerForms.
     * GET|HEAD /dealer-forms
     */
    public function index(Request $request): JsonResponse
    {
        $dealerForms = $this->dealerFormRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dealerForms->toArray(), 'Dealer Forms retrieved successfully');
    }

    /**
     * Store a newly created DealerForm in storage.
     * POST /dealer-forms
     */
    public function store(CreateDealerFormAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $dealerForm = $this->dealerFormRepository->create($input);

        return $this->sendResponse($dealerForm->toArray(), 'Dealer Form saved successfully');
    }

    /**
     * Display the specified DealerForm.
     * GET|HEAD /dealer-forms/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var DealerForm $dealerForm */
        $dealerForm = $this->dealerFormRepository->find($id);

        if (empty($dealerForm)) {
            return $this->sendError('Dealer Form not found');
        }

        return $this->sendResponse($dealerForm->toArray(), 'Dealer Form retrieved successfully');
    }

    /**
     * Update the specified DealerForm in storage.
     * PUT/PATCH /dealer-forms/{id}
     */
    public function update($id, UpdateDealerFormAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var DealerForm $dealerForm */
        $dealerForm = $this->dealerFormRepository->find($id);

        if (empty($dealerForm)) {
            return $this->sendError('Dealer Form not found');
        }

        $dealerForm = $this->dealerFormRepository->update($input, $id);

        return $this->sendResponse($dealerForm->toArray(), 'DealerForm updated successfully');
    }

    /**
     * Remove the specified DealerForm from storage.
     * DELETE /dealer-forms/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var DealerForm $dealerForm */
        $dealerForm = $this->dealerFormRepository->find($id);

        if (empty($dealerForm)) {
            return $this->sendError('Dealer Form not found');
        }

        $dealerForm->delete();

        return $this->sendSuccess('Dealer Form deleted successfully');
    }
}
