<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateManufactureAPIRequest;
use App\Http\Requests\API\UpdateManufactureAPIRequest;
use App\Models\Manufacture;
use App\Repositories\ManufactureRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class ManufactureAPIController
 */
class ManufactureAPIController extends AppBaseController
{
    private ManufactureRepository $manufactureRepository;

    public function __construct(ManufactureRepository $manufactureRepo)
    {
        $this->manufactureRepository = $manufactureRepo;
    }

    /**
     * Display a listing of the Manufactures.
     * GET|HEAD /manufactures
     */
    public function index(Request $request): JsonResponse
    {
        $manufactures = $this->manufactureRepository->getAll();

        return $this->sendResponse($manufactures, 'Manufactures retrieved successfully');
    }

    /**
     * Store a newly created Manufacture in storage.
     * POST /manufactures
     */
    public function store(CreateManufactureAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $manufacture = $this->manufactureRepository->create($input);

        return $this->sendResponse($manufacture->toArray(), 'Manufacture saved successfully');
    }

    /**
     * Display the specified Manufacture.
     * GET|HEAD /manufactures/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var Manufacture $manufacture */
        $manufacture = $this->manufactureRepository->find($id);

        if (empty($manufacture)) {
            return $this->sendError('Manufacture not found');
        }

        return $this->sendResponse($manufacture->toArray(), 'Manufacture retrieved successfully');
    }

    /**
     * Update the specified Manufacture in storage.
     * PUT/PATCH /manufactures/{id}
     */
    public function update($id, UpdateManufactureAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var Manufacture $manufacture */
        $manufacture = $this->manufactureRepository->find($id);

        if (empty($manufacture)) {
            return $this->sendError('Manufacture not found');
        }

        $manufacture = $this->manufactureRepository->update($input, $id);

        return $this->sendResponse($manufacture->toArray(), 'Manufacture updated successfully');
    }

    /**
     * Remove the specified Manufacture from storage.
     * DELETE /manufactures/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var Manufacture $manufacture */
        $manufacture = $this->manufactureRepository->find($id);

        if (empty($manufacture)) {
            return $this->sendError('Manufacture not found');
        }
        $this->manufactureRepository->delete($id);
        return $this->sendSuccess('Manufacture deleted successfully');
    }
}
