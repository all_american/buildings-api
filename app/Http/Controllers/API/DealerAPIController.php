<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealerAPIRequest;
use App\Http\Requests\API\UpdateDealerAPIRequest;
use App\Models\Dealer;
use App\Repositories\DealerRepository;
use App\Repositories\StateRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use JetBrains\PhpStorm\NoReturn;

/**
 * Class DealerAPIController
 */
class DealerAPIController extends AppBaseController
{
    private DealerRepository $dealerRepository;

    public function __construct(DealerRepository $dealerRepo)
    {
        $this->dealerRepository = $dealerRepo;
    }


    /**
     * Display a listing of the Dealers.
     * GET|HEAD /dealers
     */
    public function index(Request $request): JsonResponse
    {
        $dealers = $this->dealerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dealers->toArray(), 'Dealers retrieved successfully');
    }

    /**
     * Store a newly created Dealer in storage.
     * POST /dealers
     */
    public function store(Request $request): JsonResponse
    {
        $input = $request->all();
        $response = $this->dealerRepository->workData($input, $request);
        if($response === null){
            return $this->sendError('Error with address');
        }
        $dealer = $this->dealerRepository->create($response);

        return $this->sendResponse($dealer->toArray(), 'Dealer saved successfully');
    }

    /**
     * Display the specified Dealer.
     * GET|HEAD /dealers/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var Dealer $dealer */
        $dealer = $this->dealerRepository->find($id);

        if (empty($dealer)) {
            return $this->sendError('Dealer not found');
        }

        return $this->sendResponse($dealer->toArray(), 'Dealer retrieved successfully');
    }

    /**
     * Update the specified Dealer in storage.
     * PUT/PATCH /dealers/{id}
     */
    public function update(Request $request, $id): JsonResponse
    {
        $input = $request->all();
        dd($input);

        /** @var Dealer $dealer */
        $dealer = $this->dealerRepository->find($id);

        if (empty($dealer)) {
            return $this->sendError('Dealer not found');
        }

        $dealer = $this->dealerRepository->update($input, $id);

        return $this->sendResponse($dealer->toArray(), 'Dealer updated successfully');
    }

    /**
     * Remove the specified Dealer from storage.
     * DELETE /dealers/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var Dealer $dealer */
        $dealer = $this->dealerRepository->find($id);

        if (empty($dealer)) {
            return $this->sendError('Dealer not found');
        }

        $dealer->delete();

        return $this->sendSuccess('Dealer deleted successfully');
    }
}
