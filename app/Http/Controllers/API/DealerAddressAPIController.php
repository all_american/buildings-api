<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDealerAddressAPIRequest;
use App\Http\Requests\API\UpdateDealerAddressAPIRequest;
use App\Models\DealerAddress;
use App\Repositories\DealerAddressRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;

/**
 * Class DealerAddressAPIController
 */
class DealerAddressAPIController extends AppBaseController
{
    private DealerAddressRepository $dealerAddressRepository;

    public function __construct(DealerAddressRepository $dealerAddressRepo)
    {
        $this->dealerAddressRepository = $dealerAddressRepo;
    }

    /**
     * Display a listing of the DealerAddresses.
     * GET|HEAD /dealer-addresses
     */
    public function index(Request $request): JsonResponse
    {
        $dealerAddresses = $this->dealerAddressRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dealerAddresses->toArray(), 'Dealer Addresses retrieved successfully');
    }

    /**
     * Store a newly created DealerAddress in storage.
     * POST /dealer-addresses
     */
    public function store(CreateDealerAddressAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        $dealerAddress = $this->dealerAddressRepository->create($input);

        return $this->sendResponse($dealerAddress->toArray(), 'Dealer Address saved successfully');
    }

    /**
     * Display the specified DealerAddress.
     * GET|HEAD /dealer-addresses/{id}
     */
    public function show($id): JsonResponse
    {
        /** @var DealerAddress $dealerAddress */
        $dealerAddress = $this->dealerAddressRepository->find($id);

        if (empty($dealerAddress)) {
            return $this->sendError('Dealer Address not found');
        }

        return $this->sendResponse($dealerAddress->toArray(), 'Dealer Address retrieved successfully');
    }

    /**
     * Update the specified DealerAddress in storage.
     * PUT/PATCH /dealer-addresses/{id}
     */
    public function update($id, UpdateDealerAddressAPIRequest $request): JsonResponse
    {
        $input = $request->all();

        /** @var DealerAddress $dealerAddress */
        $dealerAddress = $this->dealerAddressRepository->find($id);

        if (empty($dealerAddress)) {
            return $this->sendError('Dealer Address not found');
        }

        $dealerAddress = $this->dealerAddressRepository->update($input, $id);

        return $this->sendResponse($dealerAddress->toArray(), 'DealerAddress updated successfully');
    }

    /**
     * Remove the specified DealerAddress from storage.
     * DELETE /dealer-addresses/{id}
     *
     * @throws \Exception
     */
    public function destroy($id): JsonResponse
    {
        /** @var DealerAddress $dealerAddress */
        $dealerAddress = $this->dealerAddressRepository->find($id);

        if (empty($dealerAddress)) {
            return $this->sendError('Dealer Address not found');
        }

        $dealerAddress->delete();

        return $this->sendSuccess('Dealer Address deleted successfully');
    }
}
