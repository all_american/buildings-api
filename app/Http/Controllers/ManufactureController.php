<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateManufactureRequest;
use App\Http\Requests\UpdateManufactureRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ManufactureRepository;
use Illuminate\Http\Request;
use Flash;

class ManufactureController extends AppBaseController
{
    /** @var ManufactureRepository $manufactureRepository*/
    private $manufactureRepository;

    public function __construct(ManufactureRepository $manufactureRepo)
    {
        $this->manufactureRepository = $manufactureRepo;
    }

    /**
     * Display a listing of the Manufacture.
     */
    public function index(Request $request)
    {
        $manufactures = $this->manufactureRepository->paginate(10);

        return view('manufactures.index')
            ->with('manufactures', $manufactures);
    }

    /**
     * Show the form for creating a new Manufacture.
     */
    public function create()
    {
        return view('manufactures.create');
    }

    /**
     * Store a newly created Manufacture in storage.
     */
    public function store(CreateManufactureRequest $request)
    {
        $input = $request->all();

        $manufacture = $this->manufactureRepository->create($input);

        Flash::success('Manufacture saved successfully.');

        return redirect(route('manufactures.index'));
    }

    /**
     * Display the specified Manufacture.
     */
    public function show($id)
    {
        $manufacture = $this->manufactureRepository->find($id);

        if (empty($manufacture)) {
            Flash::error('Manufacture not found');

            return redirect(route('manufactures.index'));
        }

        return view('manufactures.show')->with('manufacture', $manufacture);
    }

    /**
     * Show the form for editing the specified Manufacture.
     */
    public function edit($id)
    {
        $manufacture = $this->manufactureRepository->find($id);

        if (empty($manufacture)) {
            Flash::error('Manufacture not found');

            return redirect(route('manufactures.index'));
        }

        return view('manufactures.edit')->with('manufacture', $manufacture);
    }

    /**
     * Update the specified Manufacture in storage.
     */
    public function update($id, UpdateManufactureRequest $request)
    {
        $manufacture = $this->manufactureRepository->find($id);

        if (empty($manufacture)) {
            Flash::error('Manufacture not found');

            return redirect(route('manufactures.index'));
        }

        $manufacture = $this->manufactureRepository->update($request->all(), $id);

        Flash::success('Manufacture updated successfully.');

        return redirect(route('manufactures.index'));
    }

    /**
     * Remove the specified Manufacture from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $manufacture = $this->manufactureRepository->find($id);

        if (empty($manufacture)) {
            Flash::error('Manufacture not found');

            return redirect(route('manufactures.index'));
        }

        $this->manufactureRepository->delete($id);

        Flash::success('Manufacture deleted successfully.');

        return redirect(route('manufactures.index'));
    }
}
