<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDealerRequest;
use App\Http\Requests\UpdateDealerRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DealerRepository;
use Illuminate\Http\Request;
use Flash;

class DealerController extends AppBaseController
{
    /** @var DealerRepository $dealerRepository*/
    private $dealerRepository;

    public function __construct(DealerRepository $dealerRepo)
    {
        $this->dealerRepository = $dealerRepo;
    }

    /**
     * Display a listing of the Dealer.
     */
    public function index(Request $request)
    {
        $dealers = $this->dealerRepository->paginate(10);

        return view('dealers.index')
            ->with('dealers', $dealers);
    }

    /**
     * Show the form for creating a new Dealer.
     */
    public function create()
    {
        return view('dealers.create');
    }

    /**
     * Store a newly created Dealer in storage.
     */
    public function store(CreateDealerRequest $request)
    {
        $input = $request->all();

        $dealer = $this->dealerRepository->create($input);

        Flash::success('Dealer saved successfully.');

        return redirect(route('dealers.index'));
    }

    /**
     * Display the specified Dealer.
     */
    public function show($id)
    {
        $dealer = $this->dealerRepository->find($id);

        if (empty($dealer)) {
            Flash::error('Dealer not found');

            return redirect(route('dealers.index'));
        }

        return view('dealers.show')->with('dealer', $dealer);
    }

    /**
     * Show the form for editing the specified Dealer.
     */
    public function edit($id)
    {
        $dealer = $this->dealerRepository->find($id);

        if (empty($dealer)) {
            Flash::error('Dealer not found');

            return redirect(route('dealers.index'));
        }

        return view('dealers.edit')->with('dealer', $dealer);
    }

    /**
     * Update the specified Dealer in storage.
     */
    public function update($id, UpdateDealerRequest $request)
    {
        $dealer = $this->dealerRepository->find($id);

        if (empty($dealer)) {
            Flash::error('Dealer not found');

            return redirect(route('dealers.index'));
        }

        $dealer = $this->dealerRepository->update($request->all(), $id);

        Flash::success('Dealer updated successfully.');

        return redirect(route('dealers.index'));
    }

    /**
     * Remove the specified Dealer from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dealer = $this->dealerRepository->find($id);

        if (empty($dealer)) {
            Flash::error('Dealer not found');

            return redirect(route('dealers.index'));
        }

        $this->dealerRepository->delete($id);

        Flash::success('Dealer deleted successfully.');

        return redirect(route('dealers.index'));
    }
}
