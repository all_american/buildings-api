<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBuildingOrderNoteRequest;
use App\Http\Requests\UpdateBuildingOrderNoteRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BuildingOrderNoteRepository;
use Illuminate\Http\Request;
use Flash;

class BuildingOrderNoteController extends AppBaseController
{
    /** @var BuildingOrderNoteRepository $buildingOrderNoteRepository*/
    private $buildingOrderNoteRepository;

    public function __construct(BuildingOrderNoteRepository $buildingOrderNoteRepo)
    {
        $this->buildingOrderNoteRepository = $buildingOrderNoteRepo;
    }

    /**
     * Display a listing of the BuildingOrderNote.
     */
    public function index(Request $request)
    {
        $buildingOrderNotes = $this->buildingOrderNoteRepository->paginate(10);

        return view('building_order_notes.index')
            ->with('buildingOrderNotes', $buildingOrderNotes);
    }

    /**
     * Show the form for creating a new BuildingOrderNote.
     */
    public function create()
    {
        return view('building_order_notes.create');
    }

    /**
     * Store a newly created BuildingOrderNote in storage.
     */
    public function store(CreateBuildingOrderNoteRequest $request)
    {
        $input = $request->all();

        $buildingOrderNote = $this->buildingOrderNoteRepository->create($input);

        Flash::success('Building Order Note saved successfully.');

        return redirect(route('buildingOrderNotes.index'));
    }

    /**
     * Display the specified BuildingOrderNote.
     */
    public function show($id)
    {
        $buildingOrderNote = $this->buildingOrderNoteRepository->find($id);

        if (empty($buildingOrderNote)) {
            Flash::error('Building Order Note not found');

            return redirect(route('buildingOrderNotes.index'));
        }

        return view('building_order_notes.show')->with('buildingOrderNote', $buildingOrderNote);
    }

    /**
     * Show the form for editing the specified BuildingOrderNote.
     */
    public function edit($id)
    {
        $buildingOrderNote = $this->buildingOrderNoteRepository->find($id);

        if (empty($buildingOrderNote)) {
            Flash::error('Building Order Note not found');

            return redirect(route('buildingOrderNotes.index'));
        }

        return view('building_order_notes.edit')->with('buildingOrderNote', $buildingOrderNote);
    }

    /**
     * Update the specified BuildingOrderNote in storage.
     */
    public function update($id, UpdateBuildingOrderNoteRequest $request)
    {
        $buildingOrderNote = $this->buildingOrderNoteRepository->find($id);

        if (empty($buildingOrderNote)) {
            Flash::error('Building Order Note not found');

            return redirect(route('buildingOrderNotes.index'));
        }

        $buildingOrderNote = $this->buildingOrderNoteRepository->update($request->all(), $id);

        Flash::success('Building Order Note updated successfully.');

        return redirect(route('buildingOrderNotes.index'));
    }

    /**
     * Remove the specified BuildingOrderNote from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $buildingOrderNote = $this->buildingOrderNoteRepository->find($id);

        if (empty($buildingOrderNote)) {
            Flash::error('Building Order Note not found');

            return redirect(route('buildingOrderNotes.index'));
        }

        $this->buildingOrderNoteRepository->delete($id);

        Flash::success('Building Order Note deleted successfully.');

        return redirect(route('buildingOrderNotes.index'));
    }
}
