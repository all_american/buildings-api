<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDealerAddressRequest;
use App\Http\Requests\UpdateDealerAddressRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DealerAddressRepository;
use Illuminate\Http\Request;
use Flash;

class DealerAddressController extends AppBaseController
{
    /** @var DealerAddressRepository $dealerAddressRepository*/
    private $dealerAddressRepository;

    public function __construct(DealerAddressRepository $dealerAddressRepo)
    {
        $this->dealerAddressRepository = $dealerAddressRepo;
    }

    /**
     * Display a listing of the DealerAddress.
     */
    public function index(Request $request)
    {
        $dealerAddresses = $this->dealerAddressRepository->paginate(10);

        return view('dealer_addresses.index')
            ->with('dealerAddresses', $dealerAddresses);
    }

    /**
     * Show the form for creating a new DealerAddress.
     */
    public function create()
    {
        return view('dealer_addresses.create');
    }

    /**
     * Store a newly created DealerAddress in storage.
     */
    public function store(CreateDealerAddressRequest $request)
    {
        $input = $request->all();

        $dealerAddress = $this->dealerAddressRepository->create($input);

        Flash::success('Dealer Address saved successfully.');

        return redirect(route('dealerAddresses.index'));
    }

    /**
     * Display the specified DealerAddress.
     */
    public function show($id)
    {
        $dealerAddress = $this->dealerAddressRepository->find($id);

        if (empty($dealerAddress)) {
            Flash::error('Dealer Address not found');

            return redirect(route('dealerAddresses.index'));
        }

        return view('dealer_addresses.show')->with('dealerAddress', $dealerAddress);
    }

    /**
     * Show the form for editing the specified DealerAddress.
     */
    public function edit($id)
    {
        $dealerAddress = $this->dealerAddressRepository->find($id);

        if (empty($dealerAddress)) {
            Flash::error('Dealer Address not found');

            return redirect(route('dealerAddresses.index'));
        }

        return view('dealer_addresses.edit')->with('dealerAddress', $dealerAddress);
    }

    /**
     * Update the specified DealerAddress in storage.
     */
    public function update($id, UpdateDealerAddressRequest $request)
    {
        $dealerAddress = $this->dealerAddressRepository->find($id);

        if (empty($dealerAddress)) {
            Flash::error('Dealer Address not found');

            return redirect(route('dealerAddresses.index'));
        }

        $dealerAddress = $this->dealerAddressRepository->update($request->all(), $id);

        Flash::success('Dealer Address updated successfully.');

        return redirect(route('dealerAddresses.index'));
    }

    /**
     * Remove the specified DealerAddress from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dealerAddress = $this->dealerAddressRepository->find($id);

        if (empty($dealerAddress)) {
            Flash::error('Dealer Address not found');

            return redirect(route('dealerAddresses.index'));
        }

        $this->dealerAddressRepository->delete($id);

        Flash::success('Dealer Address deleted successfully.');

        return redirect(route('dealerAddresses.index'));
    }
}
