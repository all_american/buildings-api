<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRepairImageRequest;
use App\Http\Requests\UpdateRepairImageRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\RepairImageRepository;
use Illuminate\Http\Request;
use Flash;

class RepairImageController extends AppBaseController
{
    /** @var RepairImageRepository $repairImageRepository*/
    private $repairImageRepository;

    public function __construct(RepairImageRepository $repairImageRepo)
    {
        $this->repairImageRepository = $repairImageRepo;
    }

    /**
     * Display a listing of the RepairImage.
     */
    public function index(Request $request)
    {
        $repairImages = $this->repairImageRepository->paginate(10);

        return view('repair_images.index')
            ->with('repairImages', $repairImages);
    }

    /**
     * Show the form for creating a new RepairImage.
     */
    public function create()
    {
        return view('repair_images.create');
    }

    /**
     * Store a newly created RepairImage in storage.
     */
    public function store(CreateRepairImageRequest $request)
    {
        $input = $request->all();

        $repairImage = $this->repairImageRepository->create($input);

        Flash::success('Repair Image saved successfully.');

        return redirect(route('repairImages.index'));
    }

    /**
     * Display the specified RepairImage.
     */
    public function show($id)
    {
        $repairImage = $this->repairImageRepository->find($id);

        if (empty($repairImage)) {
            Flash::error('Repair Image not found');

            return redirect(route('repairImages.index'));
        }

        return view('repair_images.show')->with('repairImage', $repairImage);
    }

    /**
     * Show the form for editing the specified RepairImage.
     */
    public function edit($id)
    {
        $repairImage = $this->repairImageRepository->find($id);

        if (empty($repairImage)) {
            Flash::error('Repair Image not found');

            return redirect(route('repairImages.index'));
        }

        return view('repair_images.edit')->with('repairImage', $repairImage);
    }

    /**
     * Update the specified RepairImage in storage.
     */
    public function update($id, UpdateRepairImageRequest $request)
    {
        $repairImage = $this->repairImageRepository->find($id);

        if (empty($repairImage)) {
            Flash::error('Repair Image not found');

            return redirect(route('repairImages.index'));
        }

        $repairImage = $this->repairImageRepository->update($request->all(), $id);

        Flash::success('Repair Image updated successfully.');

        return redirect(route('repairImages.index'));
    }

    /**
     * Remove the specified RepairImage from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $repairImage = $this->repairImageRepository->find($id);

        if (empty($repairImage)) {
            Flash::error('Repair Image not found');

            return redirect(route('repairImages.index'));
        }

        $this->repairImageRepository->delete($id);

        Flash::success('Repair Image deleted successfully.');

        return redirect(route('repairImages.index'));
    }
}
