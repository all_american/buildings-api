<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReportEmailRequest;
use App\Http\Requests\UpdateReportEmailRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ReportEmailRepository;
use Illuminate\Http\Request;
use Flash;

class ReportEmailController extends AppBaseController
{
    /** @var ReportEmailRepository $reportEmailRepository*/
    private $reportEmailRepository;

    public function __construct(ReportEmailRepository $reportEmailRepo)
    {
        $this->reportEmailRepository = $reportEmailRepo;
    }

    /**
     * Display a listing of the ReportEmail.
     */
    public function index(Request $request)
    {
        $reportEmails = $this->reportEmailRepository->paginate(10);

        return view('report_emails.index')
            ->with('reportEmails', $reportEmails);
    }

    /**
     * Show the form for creating a new ReportEmail.
     */
    public function create()
    {
        return view('report_emails.create');
    }

    /**
     * Store a newly created ReportEmail in storage.
     */
    public function store(CreateReportEmailRequest $request)
    {
        $input = $request->all();

        $reportEmail = $this->reportEmailRepository->create($input);

        Flash::success('Report Email saved successfully.');

        return redirect(route('reportEmails.index'));
    }

    /**
     * Display the specified ReportEmail.
     */
    public function show($id)
    {
        $reportEmail = $this->reportEmailRepository->find($id);

        if (empty($reportEmail)) {
            Flash::error('Report Email not found');

            return redirect(route('reportEmails.index'));
        }

        return view('report_emails.show')->with('reportEmail', $reportEmail);
    }

    /**
     * Show the form for editing the specified ReportEmail.
     */
    public function edit($id)
    {
        $reportEmail = $this->reportEmailRepository->find($id);

        if (empty($reportEmail)) {
            Flash::error('Report Email not found');

            return redirect(route('reportEmails.index'));
        }

        return view('report_emails.edit')->with('reportEmail', $reportEmail);
    }

    /**
     * Update the specified ReportEmail in storage.
     */
    public function update($id, UpdateReportEmailRequest $request)
    {
        $reportEmail = $this->reportEmailRepository->find($id);

        if (empty($reportEmail)) {
            Flash::error('Report Email not found');

            return redirect(route('reportEmails.index'));
        }

        $reportEmail = $this->reportEmailRepository->update($request->all(), $id);

        Flash::success('Report Email updated successfully.');

        return redirect(route('reportEmails.index'));
    }

    /**
     * Remove the specified ReportEmail from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $reportEmail = $this->reportEmailRepository->find($id);

        if (empty($reportEmail)) {
            Flash::error('Report Email not found');

            return redirect(route('reportEmails.index'));
        }

        $this->reportEmailRepository->delete($id);

        Flash::success('Report Email deleted successfully.');

        return redirect(route('reportEmails.index'));
    }
}
