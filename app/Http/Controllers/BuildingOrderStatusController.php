<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBuildingOrderStatusRequest;
use App\Http\Requests\UpdateBuildingOrderStatusRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BuildingOrderStatusRepository;
use Illuminate\Http\Request;
use Flash;

class BuildingOrderStatusController extends AppBaseController
{
    /** @var BuildingOrderStatusRepository $buildingOrderStatusRepository*/
    private $buildingOrderStatusRepository;

    public function __construct(BuildingOrderStatusRepository $buildingOrderStatusRepo)
    {
        $this->buildingOrderStatusRepository = $buildingOrderStatusRepo;
    }

    /**
     * Display a listing of the BuildingOrderStatus.
     */
    public function index(Request $request)
    {
        $buildingOrderStatuses = $this->buildingOrderStatusRepository->paginate(10);

        return view('building_order_statuses.index')
            ->with('buildingOrderStatuses', $buildingOrderStatuses);
    }

    /**
     * Show the form for creating a new BuildingOrderStatus.
     */
    public function create()
    {
        return view('building_order_statuses.create');
    }

    /**
     * Store a newly created BuildingOrderStatus in storage.
     */
    public function store(CreateBuildingOrderStatusRequest $request)
    {
        $input = $request->all();

        $buildingOrderStatus = $this->buildingOrderStatusRepository->create($input);

        Flash::success('Building Order Status saved successfully.');

        return redirect(route('buildingOrderStatuses.index'));
    }

    /**
     * Display the specified BuildingOrderStatus.
     */
    public function show($id)
    {
        $buildingOrderStatus = $this->buildingOrderStatusRepository->find($id);

        if (empty($buildingOrderStatus)) {
            Flash::error('Building Order Status not found');

            return redirect(route('buildingOrderStatuses.index'));
        }

        return view('building_order_statuses.show')->with('buildingOrderStatus', $buildingOrderStatus);
    }

    /**
     * Show the form for editing the specified BuildingOrderStatus.
     */
    public function edit($id)
    {
        $buildingOrderStatus = $this->buildingOrderStatusRepository->find($id);

        if (empty($buildingOrderStatus)) {
            Flash::error('Building Order Status not found');

            return redirect(route('buildingOrderStatuses.index'));
        }

        return view('building_order_statuses.edit')->with('buildingOrderStatus', $buildingOrderStatus);
    }

    /**
     * Update the specified BuildingOrderStatus in storage.
     */
    public function update($id, UpdateBuildingOrderStatusRequest $request)
    {
        $buildingOrderStatus = $this->buildingOrderStatusRepository->find($id);

        if (empty($buildingOrderStatus)) {
            Flash::error('Building Order Status not found');

            return redirect(route('buildingOrderStatuses.index'));
        }

        $buildingOrderStatus = $this->buildingOrderStatusRepository->update($request->all(), $id);

        Flash::success('Building Order Status updated successfully.');

        return redirect(route('buildingOrderStatuses.index'));
    }

    /**
     * Remove the specified BuildingOrderStatus from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $buildingOrderStatus = $this->buildingOrderStatusRepository->find($id);

        if (empty($buildingOrderStatus)) {
            Flash::error('Building Order Status not found');

            return redirect(route('buildingOrderStatuses.index'));
        }

        $this->buildingOrderStatusRepository->delete($id);

        Flash::success('Building Order Status deleted successfully.');

        return redirect(route('buildingOrderStatuses.index'));
    }
}
