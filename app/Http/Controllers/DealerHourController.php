<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDealerHourRequest;
use App\Http\Requests\UpdateDealerHourRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DealerHourRepository;
use Illuminate\Http\Request;
use Flash;

class DealerHourController extends AppBaseController
{
    /** @var DealerHourRepository $dealerHourRepository*/
    private $dealerHourRepository;

    public function __construct(DealerHourRepository $dealerHourRepo)
    {
        $this->dealerHourRepository = $dealerHourRepo;
    }

    /**
     * Display a listing of the DealerHour.
     */
    public function index(Request $request)
    {
        $dealerHours = $this->dealerHourRepository->paginate(10);

        return view('dealer_hours.index')
            ->with('dealerHours', $dealerHours);
    }

    /**
     * Show the form for creating a new DealerHour.
     */
    public function create()
    {
        return view('dealer_hours.create');
    }

    /**
     * Store a newly created DealerHour in storage.
     */
    public function store(CreateDealerHourRequest $request)
    {
        $input = $request->all();

        $dealerHour = $this->dealerHourRepository->create($input);

        Flash::success('Dealer Hour saved successfully.');

        return redirect(route('dealerHours.index'));
    }

    /**
     * Display the specified DealerHour.
     */
    public function show($id)
    {
        $dealerHour = $this->dealerHourRepository->find($id);

        if (empty($dealerHour)) {
            Flash::error('Dealer Hour not found');

            return redirect(route('dealerHours.index'));
        }

        return view('dealer_hours.show')->with('dealerHour', $dealerHour);
    }

    /**
     * Show the form for editing the specified DealerHour.
     */
    public function edit($id)
    {
        $dealerHour = $this->dealerHourRepository->find($id);

        if (empty($dealerHour)) {
            Flash::error('Dealer Hour not found');

            return redirect(route('dealerHours.index'));
        }

        return view('dealer_hours.edit')->with('dealerHour', $dealerHour);
    }

    /**
     * Update the specified DealerHour in storage.
     */
    public function update($id, UpdateDealerHourRequest $request)
    {
        $dealerHour = $this->dealerHourRepository->find($id);

        if (empty($dealerHour)) {
            Flash::error('Dealer Hour not found');

            return redirect(route('dealerHours.index'));
        }

        $dealerHour = $this->dealerHourRepository->update($request->all(), $id);

        Flash::success('Dealer Hour updated successfully.');

        return redirect(route('dealerHours.index'));
    }

    /**
     * Remove the specified DealerHour from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dealerHour = $this->dealerHourRepository->find($id);

        if (empty($dealerHour)) {
            Flash::error('Dealer Hour not found');

            return redirect(route('dealerHours.index'));
        }

        $this->dealerHourRepository->delete($id);

        Flash::success('Dealer Hour deleted successfully.');

        return redirect(route('dealerHours.index'));
    }
}
