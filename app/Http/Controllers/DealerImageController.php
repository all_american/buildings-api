<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDealerImageRequest;
use App\Http\Requests\UpdateDealerImageRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DealerImageRepository;
use Illuminate\Http\Request;
use Flash;

class DealerImageController extends AppBaseController
{
    /** @var DealerImageRepository $dealerImageRepository*/
    private $dealerImageRepository;

    public function __construct(DealerImageRepository $dealerImageRepo)
    {
        $this->dealerImageRepository = $dealerImageRepo;
    }

    /**
     * Display a listing of the DealerImage.
     */
    public function index(Request $request)
    {
        $dealerImages = $this->dealerImageRepository->paginate(10);

        return view('dealer_images.index')
            ->with('dealerImages', $dealerImages);
    }

    /**
     * Show the form for creating a new DealerImage.
     */
    public function create()
    {
        return view('dealer_images.create');
    }

    /**
     * Store a newly created DealerImage in storage.
     */
    public function store(CreateDealerImageRequest $request)
    {
        $input = $request->all();

        $dealerImage = $this->dealerImageRepository->create($input);

        Flash::success('Dealer Image saved successfully.');

        return redirect(route('dealerImages.index'));
    }

    /**
     * Display the specified DealerImage.
     */
    public function show($id)
    {
        $dealerImage = $this->dealerImageRepository->find($id);

        if (empty($dealerImage)) {
            Flash::error('Dealer Image not found');

            return redirect(route('dealerImages.index'));
        }

        return view('dealer_images.show')->with('dealerImage', $dealerImage);
    }

    /**
     * Show the form for editing the specified DealerImage.
     */
    public function edit($id)
    {
        $dealerImage = $this->dealerImageRepository->find($id);

        if (empty($dealerImage)) {
            Flash::error('Dealer Image not found');

            return redirect(route('dealerImages.index'));
        }

        return view('dealer_images.edit')->with('dealerImage', $dealerImage);
    }

    /**
     * Update the specified DealerImage in storage.
     */
    public function update($id, UpdateDealerImageRequest $request)
    {
        $dealerImage = $this->dealerImageRepository->find($id);

        if (empty($dealerImage)) {
            Flash::error('Dealer Image not found');

            return redirect(route('dealerImages.index'));
        }

        $dealerImage = $this->dealerImageRepository->update($request->all(), $id);

        Flash::success('Dealer Image updated successfully.');

        return redirect(route('dealerImages.index'));
    }

    /**
     * Remove the specified DealerImage from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dealerImage = $this->dealerImageRepository->find($id);

        if (empty($dealerImage)) {
            Flash::error('Dealer Image not found');

            return redirect(route('dealerImages.index'));
        }

        $this->dealerImageRepository->delete($id);

        Flash::success('Dealer Image deleted successfully.');

        return redirect(route('dealerImages.index'));
    }
}
