<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSellerRequest;
use App\Http\Requests\UpdateSellerRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\SellerRepository;
use Illuminate\Http\Request;
use Flash;

class SellerController extends AppBaseController
{
    /** @var SellerRepository $sellerRepository*/
    private $sellerRepository;

    public function __construct(SellerRepository $sellerRepo)
    {
        $this->sellerRepository = $sellerRepo;
    }

    /**
     * Display a listing of the Seller.
     */
    public function index(Request $request)
    {
        $sellers = $this->sellerRepository->paginate(10);

        return view('sellers.index')
            ->with('sellers', $sellers);
    }

    /**
     * Show the form for creating a new Seller.
     */
    public function create()
    {
        return view('sellers.create');
    }

    /**
     * Store a newly created Seller in storage.
     */
    public function store(CreateSellerRequest $request)
    {
        $input = $request->all();

        $seller = $this->sellerRepository->create($input);

        Flash::success('Seller saved successfully.');

        return redirect(route('sellers.index'));
    }

    /**
     * Display the specified Seller.
     */
    public function show($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        return view('sellers.show')->with('seller', $seller);
    }

    /**
     * Show the form for editing the specified Seller.
     */
    public function edit($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        return view('sellers.edit')->with('seller', $seller);
    }

    /**
     * Update the specified Seller in storage.
     */
    public function update($id, UpdateSellerRequest $request)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        $seller = $this->sellerRepository->update($request->all(), $id);

        Flash::success('Seller updated successfully.');

        return redirect(route('sellers.index'));
    }

    /**
     * Remove the specified Seller from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $seller = $this->sellerRepository->find($id);

        if (empty($seller)) {
            Flash::error('Seller not found');

            return redirect(route('sellers.index'));
        }

        $this->sellerRepository->delete($id);

        Flash::success('Seller deleted successfully.');

        return redirect(route('sellers.index'));
    }
}
