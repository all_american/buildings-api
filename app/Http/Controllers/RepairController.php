<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRepairRequest;
use App\Http\Requests\UpdateRepairRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\RepairRepository;
use Illuminate\Http\Request;
use Flash;

class RepairController extends AppBaseController
{
    /** @var RepairRepository $repairRepository*/
    private $repairRepository;

    public function __construct(RepairRepository $repairRepo)
    {
        $this->repairRepository = $repairRepo;
    }

    /**
     * Display a listing of the Repair.
     */
    public function index(Request $request)
    {
        $repairs = $this->repairRepository->paginate(10);

        return view('repairs.index')
            ->with('repairs', $repairs);
    }

    /**
     * Show the form for creating a new Repair.
     */
    public function create()
    {
        return view('repairs.create');
    }

    /**
     * Store a newly created Repair in storage.
     */
    public function store(CreateRepairRequest $request)
    {
        $input = $request->all();

        $repair = $this->repairRepository->create($input);

        Flash::success('Repair saved successfully.');

        return redirect(route('repairs.index'));
    }

    /**
     * Display the specified Repair.
     */
    public function show($id)
    {
        $repair = $this->repairRepository->find($id);

        if (empty($repair)) {
            Flash::error('Repair not found');

            return redirect(route('repairs.index'));
        }

        return view('repairs.show')->with('repair', $repair);
    }

    /**
     * Show the form for editing the specified Repair.
     */
    public function edit($id)
    {
        $repair = $this->repairRepository->find($id);

        if (empty($repair)) {
            Flash::error('Repair not found');

            return redirect(route('repairs.index'));
        }

        return view('repairs.edit')->with('repair', $repair);
    }

    /**
     * Update the specified Repair in storage.
     */
    public function update($id, UpdateRepairRequest $request)
    {
        $repair = $this->repairRepository->find($id);

        if (empty($repair)) {
            Flash::error('Repair not found');

            return redirect(route('repairs.index'));
        }

        $repair = $this->repairRepository->update($request->all(), $id);

        Flash::success('Repair updated successfully.');

        return redirect(route('repairs.index'));
    }

    /**
     * Remove the specified Repair from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $repair = $this->repairRepository->find($id);

        if (empty($repair)) {
            Flash::error('Repair not found');

            return redirect(route('repairs.index'));
        }

        $this->repairRepository->delete($id);

        Flash::success('Repair deleted successfully.');

        return redirect(route('repairs.index'));
    }
}
