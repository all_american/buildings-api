<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBuildingOrderRequest;
use App\Http\Requests\UpdateBuildingOrderRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BuildingOrderRepository;
use Illuminate\Http\Request;
use Flash;

class BuildingOrderController extends AppBaseController
{
    /** @var BuildingOrderRepository $buildingOrderRepository*/
    private $buildingOrderRepository;

    public function __construct(BuildingOrderRepository $buildingOrderRepo)
    {
        $this->buildingOrderRepository = $buildingOrderRepo;
    }

    /**
     * Display a listing of the BuildingOrder.
     */
    public function index(Request $request)
    {
        $buildingOrders = $this->buildingOrderRepository->paginate(10);

        return view('building_orders.index')
            ->with('buildingOrders', $buildingOrders);
    }

    /**
     * Show the form for creating a new BuildingOrder.
     */
    public function create()
    {
        return view('building_orders.create');
    }

    /**
     * Store a newly created BuildingOrder in storage.
     */
    public function store(CreateBuildingOrderRequest $request)
    {
        $input = $request->all();

        $buildingOrder = $this->buildingOrderRepository->create($input);

        Flash::success('Building Order saved successfully.');

        return redirect(route('buildingOrders.index'));
    }

    /**
     * Display the specified BuildingOrder.
     */
    public function show($id)
    {
        $buildingOrder = $this->buildingOrderRepository->find($id);

        if (empty($buildingOrder)) {
            Flash::error('Building Order not found');

            return redirect(route('buildingOrders.index'));
        }

        return view('building_orders.show')->with('buildingOrder', $buildingOrder);
    }

    /**
     * Show the form for editing the specified BuildingOrder.
     */
    public function edit($id)
    {
        $buildingOrder = $this->buildingOrderRepository->find($id);

        if (empty($buildingOrder)) {
            Flash::error('Building Order not found');

            return redirect(route('buildingOrders.index'));
        }

        return view('building_orders.edit')->with('buildingOrder', $buildingOrder);
    }

    /**
     * Update the specified BuildingOrder in storage.
     */
    public function update($id, UpdateBuildingOrderRequest $request)
    {
        $buildingOrder = $this->buildingOrderRepository->find($id);

        if (empty($buildingOrder)) {
            Flash::error('Building Order not found');

            return redirect(route('buildingOrders.index'));
        }

        $buildingOrder = $this->buildingOrderRepository->update($request->all(), $id);

        Flash::success('Building Order updated successfully.');

        return redirect(route('buildingOrders.index'));
    }

    /**
     * Remove the specified BuildingOrder from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $buildingOrder = $this->buildingOrderRepository->find($id);

        if (empty($buildingOrder)) {
            Flash::error('Building Order not found');

            return redirect(route('buildingOrders.index'));
        }

        $this->buildingOrderRepository->delete($id);

        Flash::success('Building Order deleted successfully.');

        return redirect(route('buildingOrders.index'));
    }
}
