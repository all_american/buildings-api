<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDealerFormRequest;
use App\Http\Requests\UpdateDealerFormRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\DealerFormRepository;
use Illuminate\Http\Request;
use Flash;

class DealerFormController extends AppBaseController
{
    /** @var DealerFormRepository $dealerFormRepository*/
    private $dealerFormRepository;

    public function __construct(DealerFormRepository $dealerFormRepo)
    {
        $this->dealerFormRepository = $dealerFormRepo;
    }

    /**
     * Display a listing of the DealerForm.
     */
    public function index(Request $request)
    {
        $dealerForms = $this->dealerFormRepository->paginate(10);

        return view('dealer_forms.index')
            ->with('dealerForms', $dealerForms);
    }

    /**
     * Show the form for creating a new DealerForm.
     */
    public function create()
    {
        return view('dealer_forms.create');
    }

    /**
     * Store a newly created DealerForm in storage.
     */
    public function store(CreateDealerFormRequest $request)
    {
        $input = $request->all();

        $dealerForm = $this->dealerFormRepository->create($input);

        Flash::success('Dealer Form saved successfully.');

        return redirect(route('dealerForms.index'));
    }

    /**
     * Display the specified DealerForm.
     */
    public function show($id)
    {
        $dealerForm = $this->dealerFormRepository->find($id);

        if (empty($dealerForm)) {
            Flash::error('Dealer Form not found');

            return redirect(route('dealerForms.index'));
        }

        return view('dealer_forms.show')->with('dealerForm', $dealerForm);
    }

    /**
     * Show the form for editing the specified DealerForm.
     */
    public function edit($id)
    {
        $dealerForm = $this->dealerFormRepository->find($id);

        if (empty($dealerForm)) {
            Flash::error('Dealer Form not found');

            return redirect(route('dealerForms.index'));
        }

        return view('dealer_forms.edit')->with('dealerForm', $dealerForm);
    }

    /**
     * Update the specified DealerForm in storage.
     */
    public function update($id, UpdateDealerFormRequest $request)
    {
        $dealerForm = $this->dealerFormRepository->find($id);

        if (empty($dealerForm)) {
            Flash::error('Dealer Form not found');

            return redirect(route('dealerForms.index'));
        }

        $dealerForm = $this->dealerFormRepository->update($request->all(), $id);

        Flash::success('Dealer Form updated successfully.');

        return redirect(route('dealerForms.index'));
    }

    /**
     * Remove the specified DealerForm from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dealerForm = $this->dealerFormRepository->find($id);

        if (empty($dealerForm)) {
            Flash::error('Dealer Form not found');

            return redirect(route('dealerForms.index'));
        }

        $this->dealerFormRepository->delete($id);

        Flash::success('Dealer Form deleted successfully.');

        return redirect(route('dealerForms.index'));
    }
}
