<?php

namespace App\Http\Requests;

use App\Models\ReportEmail;
use Illuminate\Foundation\Http\FormRequest;

class UpdateReportEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = ReportEmail::$rules;
        
        return $rules;
    }
}
