<?php

namespace App\Repositories;

use App\Models\DealerForm;
use App\Repositories\BaseRepository;

class DealerFormRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'url',
        'dealer_id'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return DealerForm::class;
    }
}
