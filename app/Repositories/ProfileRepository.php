<?php

namespace App\Repositories;

use App\Models\Profile;
use App\Repositories\BaseRepository;

class ProfileRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'Description'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Profile::class;
    }
}
