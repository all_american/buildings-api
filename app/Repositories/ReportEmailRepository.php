<?php

namespace App\Repositories;

use App\Models\ReportEmail;
use App\Repositories\BaseRepository;

class ReportEmailRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'email'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return ReportEmail::class;
    }
}
