<?php

namespace App\Repositories;

use App\Models\BuildingOrderStatus;
use App\Repositories\BaseRepository;

class BuildingOrderStatusRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'building_order_id',
        'status_id',
        'user_id',
        'note'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return BuildingOrderStatus::class;
    }
}
