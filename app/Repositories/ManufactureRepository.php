<?php

namespace App\Repositories;

use App\Models\Manufacture;
use App\Repositories\BaseRepository;
class ManufactureRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Manufacture::class;
    }

    public function delete($id){
        $manufact = $this->model->find($id);
        if ($manufact->status) {
            $manufact->status = 0;
        }else{
            $manufact->status = 1;
        }
        $manufact->save();
    }

    public function getAll():array
    {
        return Manufacture::where('status', true)->get()->toArray();
    }

}
