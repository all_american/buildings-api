<?php

namespace App\Repositories;

use App\Models\DealerHour;
use App\Repositories\BaseRepository;

class DealerHourRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'day',
        'open',
        'close',
        'dealer_id'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return DealerHour::class;
    }
}
