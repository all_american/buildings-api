<?php

namespace App\Repositories;

use App\Models\RepairImage;
use App\Repositories\BaseRepository;

class RepairImageRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'repair_id',
        'image'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return RepairImage::class;
    }
}
