<?php

namespace App\Repositories;

use App\Models\Company;
use App\Repositories\BaseRepository;

class CompanyRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'full_name',
        'abbreviation',
        'address',
        'country',
        'city',
        'state',
        'zip_code',
        'phone',
        'email',
        'logo_color',
        'logo_white',
        'website'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Company::class;
    }

    public function deleteOldCompany(): void
    {
        $this->model->first()->delete();
    }
}
