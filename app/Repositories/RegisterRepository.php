<?php

namespace App\Repositories;

use App\Models\Register;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RegisterRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'phone',
        'email',
        'address',
        'build_cost',
        'document_path',
        'deposit',
        'installation_date',
        'status',
        'confirmation_number',
        'zip_code',
        'state_id',
        'seller_id',
        'manufacture_id',
        'note'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Register::class;
    }


    public function setFileOrder(array $input, Request $request): ?array{
        $disk = Storage::disk('gcs');
        if ($request->hasFile('ein_file')) {
            //Load document
            $folder = '/orders_document/';
            $file = $request->file('ein_file');
            $disk->putFile($folder, $file);
            $input['document_path'] = $disk->url('/orders_document/'.$file->hashName());
            $input['ein_file'] = null;
        }else{
            $input['ein_file'] = null;
            $input['document_path'] = null;
        }
        return $input;
    }

    public function getAllregisters(): array
    {
        return $this->model->with('seller')->with('manufacture')->with('state')->get()->toArray();
    }

    public function updateStatus(int $id): array
    {
        $register = $this->model->find($id);
        if ($register->status) {
            $register->status = 0;
        }else{
            $register->status = 1;
        }
        $register->save();
        return $register->toArray();
    }

    public function getManufatureList(): array
    {
        $list = $this->model()->with('manufacture')->with('seller')->whereDate('created_at',DB::raw('CURDATE()'))->get()->toArray();
        foreach ($list as $key => $value) {
            $list[$key]['manufacture_name'] = $value['manufacture']['name'];
            $list[$key]['seller_name'] = $value['seller']['name'];
            $list[$key]['diference'] = $value['build_cost'] - $value['deposit'];
        }
        return $list;
    }
}
