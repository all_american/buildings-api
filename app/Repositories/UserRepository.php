<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'last_name',
        'email',
        'password',
        'username',
        'token',
        'profile_id',
        'company_id'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return User::class;
    }

    public function create(array $input): \Illuminate\Database\Eloquent\Model
    {
        $user = $this->model->newInstance($input);
        $user->save();
        return $user;
    }
}
