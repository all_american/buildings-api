<?php

namespace App\Repositories;

use App\Models\BuildingOrder;
use App\Models\State;
use App\Repositories\BaseRepository;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class BuildingOrderRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'user_id',
        'dealer_id',
        'invoice_number',
        'invoice_date',
        'name',
        'email',
        'phone',
        'phone2',
        'phone3',
        'address',
        'city',
        'state_id',
        'zip',
        'width',
        'roof_length',
        'frame_length',
        'leg_height',
        'gauge',
        'price',
        'description',
        'description_price',
        'color_top',
        'color_sides',
        'color_ends',
        'color_trim',
        'non_tax_fees_desc',
        'non_tax_fees',
        'installation_address',
        'installation_city',
        'installation_state_id',
        'installation_zip',
        'total_tax',
        'total_sales',
        'tax',
        'tax_exempt',
        'total',
        'deposit',
        'balance_due',
        'special_instructions',
        'surface_level',
        'electricity_available',
        'installation_surface',
        'payment',
        'collection',
        'ready_install',
        'latitude',
        'longitude',
        'created_at',
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return BuildingOrder::class;
    }

    public function getLatitudeAndLongitude(array $input): ?array
    {
        $state = State::find($input['installation_state_id']);
        $client = new Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/geocode/json',
            'timeout' => 2.0,
        ]);
        $response = $client->get('', [
            'query' => [
                'address' => $input['installation_address'] . ', ' . $input['installation_city'] . ', ' . $state->abbreviation . ' ' . $input['installation_zip'],
                'key' => env('GOOGLE_MAPS_API_KEY')
            ]
        ]);
        $body = json_decode($response->getBody()->getContents());
        if ($body->status == 'OK') {
            $input['latitude'] = $body->results[0]->geometry->location->lat;
            $input['longitude'] = $body->results[0]->geometry->location->lng;
        } else {
            return null;
        }
        return $input;
    }

    public function getOrder($id): \Illuminate\Database\Eloquent\Builder|array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
    {
        $order = BuildingOrder::find($id);
        $order->buildingOrdersDetails;
        $order->BuildingOrdersStatus;
        return $order;
    }

    public function getOrders(): array
    {
        $list = [];
        $orders = DB::table('building_orders')
            ->join('building_orders_status', 'building_orders.id', '=', 'building_orders_status.building_order_id')
            ->join('states', 'building_orders.installation_state_id', '=', 'states.id')
            ->join('users', 'building_orders.user_id', '=', 'users.id')
            ->join('dealers', 'building_orders.dealer_id', '=', 'dealers.id')
            ->whereNull('building_orders.deleted_at')
            ->select(DB::raw('building_orders.id as id'),
                'building_orders.invoice_number',
                'building_orders.installation_address',
                'building_orders.installation_city',
                'states.abbreviation as state',
                'building_orders.installation_zip',
                'building_orders.ready_install',
                DB::raw('dealers.name as first_name'),
                DB::raw('users.name as created_by'),
                DB::raw('building_orders.name as customer'),
                DB::raw('building_orders.total as total_sales'),
                'building_orders.tax',
                DB::raw('building_orders.created_at as invoice_date'),
                DB::raw('building_orders.updated_at as date_last_updated'),
                DB::raw('WEEKOFYEAR(building_orders.created_at) AS week_create'),
                DB::raw('WEEKOFYEAR(CURDATE()) AS week_current '),
            )
            ->OrderBy('building_orders.id', 'desc')
            ->get();
        foreach ($orders as $order) {
            $user = DB::table('building_orders_status')
                ->join('status', 'status.id', '=', 'building_orders_status.status_id')
                ->join('users', 'building_orders_status.user_id', '=', 'users.id')
                ->where('building_order_id', $order->id)
                ->select(DB::raw('status.name as status'),
                    DB::raw('users.name as last_update_by'))
                ->orderByDesc('building_orders_status.id')
                ->limit(1)
                ->get();
            $append = [
                'id' => $order->id,
                'invoice_number' => $order->invoice_number,
                'installation_address' => $order->installation_address,
                'installation_city' => $order->installation_city,
                'state' => $order->state,
                'installation_zip' => $order->installation_zip,
                'ready_install' => $order->ready_install,
                'first_name' => $order->first_name,
                'created_by' => $order->created_by,
                'customer' => $order->customer,
                'total_sales' => $order->total_sales,
                'tax' => $order->tax,
                'invoice_date' => $order->invoice_date,
                'date_last_updated' => $order->date_last_updated,
                'week_create' => $order->week_create,
                'week_current' => $order->week_current,
                'status' => $user[0]->status,
                'last_update_by' => $user[0]->last_update_by,
            ];
            $list[] = $append;
        }
        return $list;
    }

    public function getOrdersByStatus($status): array
    {
        $list = [];
        foreach ($this->getOrders() as $order) {
            if ($order['status'] == $status) {
                $list[] = $order;
            }
        }
        return $list;
    }

    public function getOrdersMap():array
    {
        $list = [];
        $order = DB::table('building_orders')
            ->join('states', 'building_orders.installation_state_id', '=', 'states.id')
            ->join('dealers', 'building_orders.dealer_id', '=', 'dealers.id')
            ->join('users', 'building_orders.user_id', '=', 'users.id')
            ->whereNull('building_orders.deleted_at')
            ->select(DB::raw('building_orders.id as id'),
                'building_orders.invoice_number',
                DB::raw('dealers.name as dealer'),
                'building_orders.installation_address',
                'building_orders.installation_city',
                'states.abbreviation as state',
                'building_orders.installation_zip',
                DB::raw('users.name as name'),
                'building_orders.total',
                'building_orders.created_at',
                'building_orders.latitude',
                'building_orders.longitude'
            )
            ->OrderBy('building_orders.id', 'desc')
            ->get();
        foreach ($order as $item) {
            $list[] = [
                'id' => $item->id,
                'order' => $item->invoice_number,
                'dealer' => $item->dealer,
                'address' => $item->installation_address,
                'city' => $item->installation_city,
                'state' => $item->state,
                'zip' => $item->installation_zip,
                'name' => $item->name,
                'total' => $item->total,
                'created_at' => $item->created_at,
                'latitude' => $item->latitude,
                'longitude' => $item->longitude,
            ];
        }
        return $list;
    }
}
