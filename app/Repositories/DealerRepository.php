<?php

namespace App\Repositories;

use App\Models\Dealer;
use App\Models\State;
use App\Repositories\BaseRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DealerRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'principal_address',
        'city',
        'state_id',
        'zip_code',
        'email',
        'phone',
        'website',
        'commission',
        'owner_name',
        'fax_number',
        'coverage_miles',
        'type',
        'years_in_business',
        'number_of_locations',
        'ein_number',
        'ein_file',
        'logo',
        'latitude',
        'longitude'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Dealer::class;
    }

    public function workData(array $input, Request $request): ?array
    {
        $state = State::find($input['state_id']);
        $client = new Client([
            'base_uri' => 'https://maps.googleapis.com/maps/api/geocode/json',
            'timeout' => 2.0,
        ]);
        $response = $client->get('', [
            'query' => [
                'address' => $input['principal_address'] . ', ' . $input['city'] . ', ' . $state->abbreviation . ' ' . $input['zip_code'],
                'key' => env('GOOGLE_MAPS_API_KEY')
            ]
        ]);
        $body = json_decode($response->getBody()->getContents());
        if($body->status == 'OK') {
            $input['latitude'] = $body->results[0]->geometry->location->lat;
            $input['longitude'] = $body->results[0]->geometry->location->lng;
        }else{
            return null;
        }
        $disk = Storage::disk('gcs');
        if ($request->hasFile('ein_file')) {
            $folder = '/dealers/';
            $file = $request->file('ein_file');
            $disk->putFile($folder, $file);
            $input['ein_file'] = $disk->url('/dealers/'.$file->hashName());
        }else{
            $input['ein_file'] = null;
        }
        return $input;
    }

}
