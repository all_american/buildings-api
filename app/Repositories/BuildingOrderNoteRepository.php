<?php

namespace App\Repositories;

use App\Models\BuildingOrderNote;
use App\Repositories\BaseRepository;

class BuildingOrderNoteRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'user_id',
        'building_order_id',
        'note',
        'type',
        'document'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return BuildingOrderNote::class;
    }
}
