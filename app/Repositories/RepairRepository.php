<?php

namespace App\Repositories;

use App\Models\Repair;
use App\Repositories\BaseRepository;

class RepairRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'user_id',
        'building_order_id',
        'customer_statement',
        'dealer_statement',
        'contractor_statement',
        'repair_details',
        'total'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Repair::class;
    }
}
