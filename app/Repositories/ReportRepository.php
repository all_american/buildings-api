<?php

namespace App\Repositories;

use App\Models\Register;
use App\Models\Report;
use App\Repositories\BaseRepository;

class ReportRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'path'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Report::class;
    }

    public function getReport():array
    {
        return Register::join('sellers', 'sellers.id', '=', 'register.seller_id')
            ->join('manufactures', 'manufactures.id', '=', 'register.manufacture_id')
            ->join('states', 'states.id', '=', 'register.state_id')
            ->selectRaw('register.name','states.name as state_name', 'register.created_at', 'sellers.name as seller_name', 'manufactures.name as manufacture_name', 'register.build_cost', 'register.deposit', 'register.build_cost - register.deposit as Difference', 'register.note')
            ->get()
            ->toArray();
    }

    public function getHeaderReport():array
    {
        return ['Name', 'State', 'Date', 'Seller', 'Manufacture', 'Build Cost', 'Deposit', 'Difference', 'Note'];
    }

    public function getHeaderInfoReport():array
    {
        return Register::selectRaw('count(*) as total_registers', 'sum(build_cost) as total_build_cost', 'sum(deposit) as total_deposit', 'sum(build_cost) - sum(deposit) as total_difference')
            ->get()
            ->toArray();
    }
}
