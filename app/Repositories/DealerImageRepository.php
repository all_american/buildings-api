<?php

namespace App\Repositories;

use App\Models\DealerImage;
use App\Repositories\BaseRepository;

class DealerImageRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'url',
        'dealer_id'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return DealerImage::class;
    }
}
