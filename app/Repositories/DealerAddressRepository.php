<?php

namespace App\Repositories;

use App\Models\DealerAddress;
use App\Repositories\BaseRepository;

class DealerAddressRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'address',
        'city',
        'state_id',
        'zip_code',
        'dealer_id'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return DealerAddress::class;
    }
}
