<?php

namespace App\Repositories;

use App\Models\Seller;
use App\Repositories\BaseRepository;

class SellerRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'name',
        'status'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Seller::class;
    }

    public function delete($id){
        $seller = $this->model->find($id);
        if ($seller->status) {
            $seller->status = 0;
        }else{
            $seller->status = 1;
        }
        $seller->save();
    }

    public function getAll():array
    {
        return Seller::where('status', true)->get()->toArray();
    }
}
