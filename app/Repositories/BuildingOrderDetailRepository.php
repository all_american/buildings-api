<?php

namespace App\Repositories;

use App\Models\BuildingOrderDetail;
use App\Repositories\BaseRepository;

class BuildingOrderDetailRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'building_order_id',
        'item',
        'price'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return BuildingOrderDetail::class;
    }
}
