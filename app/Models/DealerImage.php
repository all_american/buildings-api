<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealerImage extends Model
{
    public $table = 'dealers_images';

    public $fillable = [
        'name',
        'url',
        'dealer_id'
    ];

    protected $casts = [
        'name' => 'string',
        'url' => 'string'
    ];

    public static $rules = [
        'name' => 'required|string',
        'url' => 'required|string',
        'dealer_id' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function dealer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Dealer::class, 'dealer_id');
    }
}
