<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingOrderStatus extends Model
{
    public $table = 'building_orders_status';

    public $fillable = [
        'building_order_id',
        'status_id',
        'user_id',
        'note'
    ];

    protected $casts = [
        'note' => 'string'
    ];

    public static $rules = [
        'building_order_id' => 'required',
        'status_id' => 'required',
        'user_id' => 'required',
        'note' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function buildingOrder(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\BuildingOrder::class, 'building_order_id');
    }

    public function status(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Status::class, 'status_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
