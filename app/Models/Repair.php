<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Repair extends Model
{
    public $table = 'repairs';

    public $fillable = [
        'user_id',
        'building_order_id',
        'customer_statement',
        'dealer_statement',
        'contractor_statement',
        'repair_details',
        'total'
    ];

    protected $casts = [
        'customer_statement' => 'string',
        'dealer_statement' => 'string',
        'contractor_statement' => 'string',
        'repair_details' => 'string'
    ];

    public static $rules = [
        'user_id' => 'required',
        'building_order_id' => 'required',
        'customer_statement' => 'nullable|string',
        'dealer_statement' => 'nullable|string',
        'contractor_statement' => 'nullable|string',
        'repair_details' => 'nullable|string',
        'total' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function buildingOrder(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\BuildingOrder::class, 'building_order_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function repairsImages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\RepairsImage::class, 'repair_id');
    }
}
