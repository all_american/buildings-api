<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    public $table = 'manufacture';

    public $fillable = [
        'name',
        'status'
    ];

    protected $casts = [
        'name' => 'string',
        'status' => 'boolean'
    ];

    public static $rules = [
        'name' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'status' => 'nullable|boolean',
        'deleted_at' => 'nullable'
    ];

    public function registers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Register::class, 'manufacture_id');
    }
}
