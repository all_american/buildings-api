<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dealer extends Model
{
    public $table = 'dealers';

    public $fillable = [
        'name',
        'principal_address',
        'city',
        'state_id',
        'zip_code',
        'email',
        'phone',
        'website',
        'commission',
        'owner_name',
        'fax_number',
        'coverage_miles',
        'type',
        'years_in_business',
        'number_of_locations',
        'ein_number',
        'ein_file',
        'logo',
        'latitude',
        'longitude'
    ];

    protected $casts = [
        'name' => 'string',
        'principal_address' => 'string',
        'city' => 'string',
        'zip_code' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'website' => 'string',
        'owner_name' => 'string',
        'fax_number' => 'string',
        'type' => 'string',
        'ein_number' => 'string',
        'ein_file' => 'string',
        'logo' => 'string'
    ];

    public static $rules = [
        'name' => 'required|string',
        'principal_address' => 'required|string',
        'city' => 'required|string',
        'state_id' => 'required',
        'zip_code' => 'required|string',
        'email' => 'required|string',
        'phone' => 'required|string',
        'website' => 'nullable|string',
        'commission' => 'required',
        'owner_name' => 'nullable|string',
        'fax_number' => 'nullable|string',
        'coverage_miles' => 'required',
        'type' => 'required|string',
        'years_in_business' => 'nullable',
        'number_of_locations' => 'nullable',
        'ein_number' => 'required|string',
        'ein_file' => 'nullable|string',
        'logo' => 'nullable|string',
        'latitude' => 'nullable',
        'longitude' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\State::class, 'state_id');
    }

    public function buildingOrders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\BuildingOrder::class, 'dealer_id');
    }

    public function dealersAddresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\DealersAddress::class, 'dealer_id');
    }

    public function dealersForms(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\DealersForm::class, 'dealer_id');
    }

    public function dealersHours(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\DealersHour::class, 'dealer_id');
    }

    public function dealersImages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\DealersImage::class, 'dealer_id');
    }
}
