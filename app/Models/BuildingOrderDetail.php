<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingOrderDetail extends Model
{
    public $table = 'building_orders_details';

    public $fillable = [
        'building_order_id',
        'item',
        'price'
    ];

    protected $casts = [
        'item' => 'string'
    ];

    public static $rules = [
        'building_order_id' => 'required',
        'item' => 'nullable|string',
        'price' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function buildingOrder(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\BuildingOrder::class, 'building_order_id');
    }
}
