<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BuildingOrderDetail;
use App\Models\BuildingOrderStatus;
use App\Models\BuildingOrderNote;
use App\Models\Repair;
use App\Models\User;
use App\Models\Dealer;
use App\Models\State;

class BuildingOrder extends Model
{
    public $table = 'building_orders';

    public $fillable = [
        'user_id',
        'dealer_id',
        'invoice_number',
        'invoice_date',
        'name',
        'email',
        'phone',
        'phone2',
        'phone3',
        'address',
        'city',
        'state_id',
        'zip',
        'width',
        'roof_length',
        'frame_length',
        'leg_height',
        'gauge',
        'price',
        'description',
        'description_price',
        'color_top',
        'color_sides',
        'color_ends',
        'color_trim',
        'non_tax_fees_desc',
        'non_tax_fees',
        'installation_address',
        'installation_city',
        'installation_state_id',
        'installation_zip',
        'total_tax',
        'total_sales',
        'tax',
        'tax_exempt',
        'total',
        'deposit',
        'balance_due',
        'special_instructions',
        'surface_level',
        'electricity_available',
        'installation_surface',
        'payment',
        'collection',
        'ready_install',
        'latitude',
        'longitude',
        'created_at',
    ];

    protected $casts = [
        'invoice_date' => 'date',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'phone2' => 'string',
        'phone3' => 'string',
        'address' => 'string',
        'city' => 'string',
        'zip' => 'string',
        'gauge' => 'string',
        'description' => 'string',
        'color_top' => 'string',
        'color_sides' => 'string',
        'color_ends' => 'string',
        'color_trim' => 'string',
        'non_tax_fees_desc' => 'string',
        'installation_address' => 'string',
        'installation_city' => 'string',
        'installation_zip' => 'string',
        'tax_exempt' => 'string',
        'special_instructions' => 'string',
        'surface_level' => 'string',
        'electricity_available' => 'string',
        'installation_surface' => 'string',
        'payment' => 'string',
        'collection' => 'boolean',
        'ready_install' => 'string'
    ];

    public static $rules = [
        'user_id' => 'required',
        'dealer_id' => 'required',
        'invoice_number' => 'required',
        'invoice_date' => 'nullable',
        'name' => 'required|string',
        'email' => 'required|string',
        'phone' => 'required|string',
        'phone2' => 'nullable|string',
        'phone3' => 'nullable|string',
        'address' => 'required|string',
        'city' => 'required|string',
        'state_id' => 'required',
        'zip' => 'required|string',
        'width' => 'required',
        'roof_length' => 'required',
        'frame_length' => 'required',
        'leg_height' => 'required',
        'gauge' => 'nullable|string',
        'price' => 'required',
        'description' => 'required|string',
        'description_price' => 'required',
        'color_top' => 'nullable|string',
        'color_sides' => 'nullable|string',
        'color_ends' => 'nullable|string',
        'color_trim' => 'nullable|string',
        'non_tax_fees_desc' => 'nullable|string',
        'non_tax_fees' => 'nullable',
        'installation_address' => 'required|string',
        'installation_city' => 'required|string',
        'installation_state_id' => 'required',
        'installation_zip' => 'required|string',
        'total_tax' => 'required',
        'total_sales' => 'required',
        'tax' => 'required',
        'tax_exempt' => 'nullable|string',
        'total' => 'required',
        'deposit' => 'required',
        'balance_due' => 'required',
        'special_instructions' => 'nullable|string',
        'surface_level' => 'nullable|string',
        'electricity_available' => 'nullable|string',
        'installation_surface' => 'nullable|string',
        'payment' => 'nullable|string',
        'collection' => 'required|boolean',
        'ready_install' => 'nullable|string',
        'latitude' => 'nullable',
        'longitude' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function dealer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Dealer::class, 'dealer_id');
    }

    public function installationState(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(State::class, 'installation_state_id');
    }

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function buildingOrdersDetails(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(BuildingOrderDetail::class, 'building_order_id');
    }

    public function buildingOrdersNotes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(BuildingOrderNote::class, 'building_order_id');
    }

    public function buildingOrdersStatuses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(BuildingOrderStatus::class, 'building_order_id');
    }

    public function repairs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Repair::class, 'building_order_id');
    }
}
