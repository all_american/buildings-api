<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingOrderNote extends Model
{
    public $table = 'building_orders_notes';

    public $fillable = [
        'user_id',
        'building_order_id',
        'note',
        'type',
        'document'
    ];

    protected $casts = [
        'note' => 'string',
        'type' => 'string',
        'document' => 'string'
    ];

    public static $rules = [
        'user_id' => 'required',
        'building_order_id' => 'required',
        'note' => 'required|string',
        'type' => 'nullable|string',
        'document' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function buildingOrder(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\BuildingOrder::class, 'building_order_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
