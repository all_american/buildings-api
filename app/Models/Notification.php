<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public $table = 'notifications';

    public $fillable = [
        'title',
        'description',
        'icon',
        'color',
        'user_id'
    ];

    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'icon' => 'string',
        'color' => 'string'
    ];

    public static $rules = [
        'title' => 'required|string',
        'description' => 'required|string',
        'icon' => 'required|string',
        'color' => 'required|string',
        'user_id' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
