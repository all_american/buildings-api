<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportEmail extends Model
{
    public $table = 'report_email';

    public $fillable = [
        'email'
    ];

    protected $casts = [
        'email' => 'string'
    ];

    public static $rules = [
        'email' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
