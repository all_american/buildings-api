<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealerAddress extends Model
{
    public $table = 'dealers_address';

    public $fillable = [
        'name',
        'address',
        'city',
        'state_id',
        'zip_code',
        'dealer_id'
    ];

    protected $casts = [
        'name' => 'string',
        'address' => 'string',
        'city' => 'string',
        'zip_code' => 'string'
    ];

    public static $rules = [
        'name' => 'required|string',
        'address' => 'required|string',
        'city' => 'required|string',
        'state_id' => 'required',
        'zip_code' => 'required|string',
        'dealer_id' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function dealer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Dealer::class, 'dealer_id');
    }

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\State::class, 'state_id');
    }
}
