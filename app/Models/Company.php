<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $table = 'company';

    public $fillable = [
        'full_name',
        'abbreviation',
        'address',
        'country',
        'city',
        'state',
        'zip_code',
        'phone',
        'email',
        'logo_color',
        'logo_white',
        'website'
    ];

    protected $casts = [
        'full_name' => 'string',
        'abbreviation' => 'string',
        'address' => 'string',
        'country' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zip_code' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'logo_color' => 'string',
        'logo_white' => 'string',
        'website' => 'string'
    ];

    public static $rules = [
        'full_name' => 'required|string',
        'abbreviation' => 'required|string',
        'address' => 'required|string',
        'country' => 'required|string',
        'city' => 'required|string',
        'state' => 'required|string',
        'zip_code' => 'required|string',
        'phone' => 'nullable|string',
        'email' => 'nullable|string',
        'logo_color' => 'nullable|string',
        'logo_white' => 'nullable|string',
        'website' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\User::class, 'company_id');
    }
}
