<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepairImage extends Model
{
    public $table = 'repairs_images';

    public $fillable = [
        'repair_id',
        'image'
    ];

    protected $casts = [
        'image' => 'string'
    ];

    public static $rules = [
        'repair_id' => 'required',
        'image' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function repair(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Repair::class, 'repair_id');
    }
}
