<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    public $table = 'register';

    public $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'build_cost',
        'document_path',
        'deposit',
        'installation_date',
        'status',
        'confirmation_number',
        'zip_code',
        'state_id',
        'seller_id',
        'manufacture_id',
        'note'
    ];

    protected $casts = [
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'address' => 'string',
        'document_path' => 'string',
        'installation_date' => 'date',
        'status' => 'boolean',
        'confirmation_number' => 'string',
        'zip_code' => 'string',
        'note' => 'string'
    ];

    public static $rules = [
        'name' => 'nullable|string',
        'phone' => 'nullable|string',
        'email' => 'nullable|string',
        'address' => 'nullable|string',
        'build_cost' => 'nullable',
        'document_path' => 'nullable|string',
        'deposit' => 'nullable',
        'installation_date' => 'nullable',
        'status' => 'required|boolean',
        'confirmation_number' => 'nullable|string',
        'zip_code' => 'nullable|string',
        'state_id' => 'nullable',
        'seller_id' => 'nullable',
        'manufacture_id' => 'nullable',
        'note' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function manufacture(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Manufacture::class, 'manufacture_id');
    }

    public function seller(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Seller::class, 'seller_id');
    }

    public function state(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\State::class, 'state_id');
    }
}
