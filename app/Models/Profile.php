<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public $table = 'profiles';

    public $fillable = [
        'name',
        'Description'
    ];

    protected $casts = [
        'name' => 'string',
        'Description' => 'string'
    ];

    public static $rules = [
        'name' => 'required|string',
        'Description' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function users(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\User::class, 'profile_id');
    }
}
