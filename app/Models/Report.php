<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public $table = 'reports';

    public $fillable = [
        'name',
        'path'
    ];

    protected $casts = [
        'name' => 'string',
        'path' => 'string'
    ];

    public static $rules = [
        'name' => 'required|string',
        'path' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
