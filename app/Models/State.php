<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $table = 'states';

    public $fillable = [
        'name',
        'abbreviation'
    ];

    protected $casts = [
        'name' => 'string',
        'abbreviation' => 'string'
    ];

    public static $rules = [
        'name' => 'required|string',
        'abbreviation' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function buildingOrders(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\BuildingOrder::class, 'installation_state_id');
    }

    public function buildingOrder1s(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\BuildingOrder::class, 'state_id');
    }

    public function dealers(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\Dealer::class, 'state_id');
    }

    public function dealersAddresses(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\App\Models\DealersAddress::class, 'state_id');
    }
}
