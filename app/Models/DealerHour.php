<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DealerHour extends Model
{
    public $table = 'dealers_hours';

    public $fillable = [
        'day',
        'open',
        'close',
        'dealer_id'
    ];

    protected $casts = [
        'day' => 'string',
        'open' => 'string',
        'close' => 'string'
    ];

    public static $rules = [
        'day' => 'required|string',
        'open' => 'required|string',
        'close' => 'required|string',
        'dealer_id' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function dealer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Dealer::class, 'dealer_id');
    }
}
