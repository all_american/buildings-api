<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('abbreviation');
            $table->string('address');
            $table->string('country');
            $table->string('city');
            $table->string('state');
            $table->string('zip_code');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('logo_color')->nullable();
            $table->string('logo_white')->nullable();
            $table->string('website')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('Description');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('last_name');
            $table->string('email');
            $table->string('password');
            $table->string('username');
            $table->foreignId('profile_id')->constrained('profiles')->cascadeOnDelete();
            $table->foreignId('company_id')->constrained('company')->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('description');
            $table->string('icon');
            $table->string('color');
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('states', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('abbreviation');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('dealers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('principal_address');
            $table->string('city');
            $table->foreignId('state_id')->constrained('states')->cascadeOnDelete();
            $table->string('zip_code');
            $table->string('email');
            $table->string('phone');
            $table->string('website')->nullable();
            $table->decimal('commission', 8, 2);
            $table->string('owner_name')->nullable();
            $table->string('fax_number')->nullable();
            $table->decimal('coverage_miles', 8, 2);
            $table->string('type');
            $table->integer('years_in_business')->nullable();
            $table->integer('number_of_locations')->nullable();
            $table->string('ein_number');
            $table->string('ein_file')->nullable();
            $table->string('logo')->nullable();
            $table->decimal('latitude', 11, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('dealers_address', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('address');
            $table->string('city');
            $table->foreignId('state_id')->constrained('states')->cascadeOnDelete();
            $table->string('zip_code');
            $table->foreignId('dealer_id')->constrained('dealers')->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('dealers_forms', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('url');
            $table->foreignId('dealer_id')->constrained('dealers')->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('dealers_images', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('url');
            $table->foreignId('dealer_id')->constrained('dealers')->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('dealers_hours', function (Blueprint $table) {
            $table->id();
            $table->string('day');
            $table->string('open');
            $table->string('close');
            $table->foreignId('dealer_id')->constrained('dealers')->cascadeOnDelete();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('building_orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->foreignId('dealer_id')->constrained('dealers')->cascadeOnDelete();
            $table->integer('invoice_number');
            $table->date('invoice_date')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('phone2')->nullable();
            $table->string('phone3')->nullable();
            $table->string('address');
            $table->string('city');
            $table->foreignId('state_id')->constrained('states')->cascadeOnDelete();
            $table->string('zip');
            $table->decimal('width', 8, 2);
            $table->decimal('roof_length', 8, 2);
            $table->decimal('frame_length', 8, 2);
            $table->decimal('leg_height', 8, 2);
            $table->string('gauge')->nullable();
            $table->decimal('price', 8, 2);
            $table->string('description');
            $table->decimal('description_price', 8, 2);
            $table->string('color_top')->nullable();
            $table->string('color_sides')->nullable();
            $table->string('color_ends')->nullable();
            $table->string('color_trim')->nullable();
            $table->string('non_tax_fees_desc')->nullable();
            $table->decimal('non_tax_fees', 8, 2)->nullable();
            $table->string('installation_address');
            $table->string('installation_city');
            $table->foreignId('installation_state_id')->constrained('states')->cascadeOnDelete();
            $table->string('installation_zip');
            $table->decimal('total_tax', 8, 2)->default(0);
            $table->decimal('total_sales', 8, 2)->default(0);
            $table->decimal('tax', 8, 2)->default(0);
            $table->string('tax_exempt')->nullable();
            $table->decimal('total', 8, 2)->default(0);
            $table->decimal('deposit', 8, 2)->default(0);
            $table->decimal('balance_due', 8, 2)->default(0);
            $table->text('special_instructions')->nullable();
            $table->string('surface_level')->nullable();
            $table->string('electricity_available')->nullable();
            $table->string('installation_surface')->nullable();
            $table->string('payment')->nullable();
            $table->boolean('collection')->default(false);
            $table->string('ready_install')->nullable();
            $table->decimal('latitude', 8, 2)->nullable();
            $table->decimal('longitude', 8, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('building_orders_notes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->foreignId('building_order_id')->constrained('building_orders')->cascadeOnDelete();
            $table->text('note');
            $table->string('type')->nullable();
            $table->string('document')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('building_orders_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('building_order_id')->constrained('building_orders')->cascadeOnDelete();
            $table->string('item')->nullable();
            $table->decimal('price', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('status', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('building_orders_status', function (Blueprint $table) {
            $table->id();
            $table->foreignId('building_order_id')->constrained('building_orders')->cascadeOnDelete();
            $table->foreignId('status_id')->constrained('status')->cascadeOnDelete();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->string('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('repairs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();
            $table->foreignId('building_order_id')->constrained('building_orders')->cascadeOnDelete();
            $table->string('customer_statement')->nullable();
            $table->string('dealer_statement')->nullable();
            $table->string('contractor_statement')->nullable();
            $table->string('repair_details')->nullable();
            $table->decimal('total', 8, 2)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('repairs_images', function (Blueprint $table) {
            $table->id();
            $table->foreignId('repair_id')->constrained('repairs')->cascadeOnDelete();
            $table->string('image');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::dropIfExists('profiles');
        Schema::dropIfExists('company');
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('states');
        Schema::dropIfExists('dealers');
        Schema::dropIfExists('dealers_address');
        Schema::dropIfExists('dealers_forms');
        Schema::dropIfExists('dealers_images');
        Schema::dropIfExists('dealers_hours');
        Schema::dropIfExists('building_orders');
        Schema::dropIfExists('building_orders_notes');
        Schema::dropIfExists('building_orders_details');
        Schema::dropIfExists('status');
        Schema::dropIfExists('building_orders_status');
        Schema::dropIfExists('repairs');
        Schema::dropIfExists('repairs_images');
        Schema::enableForeignKeyConstraints();
    }
};
