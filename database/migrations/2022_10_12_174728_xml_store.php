<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->softDeletes();
        });

        Schema::create('manufacture', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
            $table->boolean('status')->default(true);
            $table->softDeletes();
        });

        Schema::create('register', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->decimal('build_cost')->nullable();
            $table->string('document_path')->nullable();
            $table->decimal('deposit')->nullable();
            $table->date('installation_date')->nullable();
            $table->boolean('status')->default(0);
            $table->string('confirmation_number')->nullable();
            $table->string('zip_code')->nullable();
            $table->foreignId('state_id')->nullable()->constrained('states')->cascadeOnDelete();
            $table->foreignId('seller_id')->nullable()->constrained('seller')->cascadeOnDelete();
            $table->foreignId('manufacture_id')->nullable()->constrained('manufacture')->cascadeOnDelete();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('report_email', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('reports', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('path');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('register');
        Schema::dropIfExists('seller');
        Schema::dropIfExists('manufacture');
        Schema::dropIfExists('report_email');
        Schema::dropIfExists('reports');
        Schema::enableForeignKeyConstraints();
    }
};
