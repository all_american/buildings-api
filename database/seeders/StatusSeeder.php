<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            'name' => 'Quote',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('status')->insert([
            'name' => 'In Shop',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('status')->insert([
            'name' => 'Installing',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('status')->insert([
            'name' => 'Installed',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('status')->insert([
            'name' => 'Canceled',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('status')->insert([
            'name' => 'On Hold',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('status')->insert([
            'name' => 'Collections',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
