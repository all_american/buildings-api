<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'name' => 'Alabama',
            'abbreviation' => 'AL',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Alaska',
            'abbreviation' => 'AK',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Arizona',
            'abbreviation' => 'AZ',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Arkansas',
            'abbreviation' => 'AR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'California',
            'abbreviation' => 'CA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Colorado',
            'abbreviation' => 'CO',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Connecticut',
            'abbreviation' => 'CT',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Delaware',
            'abbreviation' => 'DE',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Florida',
            'abbreviation' => 'FL',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Georgia',
            'abbreviation' => 'GA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Hawaii',
            'abbreviation' => 'HI',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Idaho',
            'abbreviation' => 'ID',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Illinois',
            'abbreviation' => 'IL',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Indiana',
            'abbreviation' => 'IN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Iowa',
            'abbreviation' => 'IA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Kansas',
            'abbreviation' => 'KS',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Kentucky',
            'abbreviation' => 'KY',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Louisiana',
            'abbreviation' => 'LA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Maine',
            'abbreviation' => 'ME',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Maryland',
            'abbreviation' => 'MD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Massachusetts',
            'abbreviation' => 'MA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Michigan',
            'abbreviation' => 'MI',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Minnesota',
            'abbreviation' => 'MN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Mississippi',
            'abbreviation' => 'MS',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Missouri',
            'abbreviation' => 'MO',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Montana',
            'abbreviation' => 'MT',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Nebraska',
            'abbreviation' => 'NE',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Nevada',
            'abbreviation' => 'NV',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'New Hampshire',
            'abbreviation' => 'NH',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'New Jersey',
            'abbreviation' => 'NJ',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'New Mexico',
            'abbreviation' => 'NM',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'New York',
            'abbreviation' => 'NY',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'North Carolina',
            'abbreviation' => 'NC',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'North Dakota',
            'abbreviation' => 'ND',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Ohio',
            'abbreviation' => 'OH',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Oklahoma',
            'abbreviation' => 'OK',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Oregon',
            'abbreviation' => 'OR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Pennsylvania',
            'abbreviation' => 'PA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Rhode Island',
            'abbreviation' => 'RI',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'South Carolina',
            'abbreviation' => 'SC',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'South Dakota',
            'abbreviation' => 'SD',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Tennessee',
            'abbreviation' => 'TN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Texas',
            'abbreviation' => 'TX',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Utah',
            'abbreviation' => 'UT',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Vermont',
            'abbreviation' => 'VT',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Virginia',
            'abbreviation' => 'VA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Washington',
            'abbreviation' => 'WA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'West Virginia',
            'abbreviation' => 'WV',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Wisconsin',
            'abbreviation' => 'WI',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('states')->insert([
            'name' => 'Wyoming',
            'abbreviation' => 'WY',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
