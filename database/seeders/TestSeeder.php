<?php

namespace Database\Seeders;

use App\Models\Manufacture;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company')->insert([
            "full_name" => "Test Company",
            "address" => "Address 200 Olive St",
            "abbreviation" => "TC",
            "logo_white" => "https://storage.googleapis.com/allamericanmbs/assets/fc%20Steel%20Structures-White.png",
            "logo_color" => "https://storage.googleapis.com/allamericanmbs/assets/fc%20Steel%20Structures-Color.png",
            "country" => "US",
            "city" => "Maxwell",
            "state" => "CA",
            "zip_code" => "95955",
            "phone" => "530-864-2000",
            "email" => "test@gmail.com",
            "website" => "https://www.allamericanmbs.com/",
            "created_at" => now(),
            "updated_at" => now(),
        ]);

        DB::table('users')->insert([
            "name" => "Victor",
            "last_name" => "Rojas",
            "username" => "vrojas1015",
            "email" => "victor@grupocomunicado.com",
            "password" => Hash::make("Victor971015"),
            "company_id" => 1,
            "profile_id" => 1,
            "created_at" => now(),
            "updated_at" => now(),
        ]);

        DB::table('seller')->insert([
            "name" => "Seller Victor",
            "created_at" => now(),
            "updated_at" => now(),
        ]);

        DB::table('seller')->insert([
            "name" => "Seller Gus",
            "created_at" => now(),
            "updated_at" => now(),
        ]);

        DB::table('seller')->insert([
            "name" => "Seller Fabian",
            "created_at" => now(),
            "updated_at" => now(),
        ]);

        DB::table('manufacture')->insert([
            "name" => "Manufacture Victor",
            "created_at" => now(),
            "updated_at" => now(),
        ]);

        DB::table('manufacture')->insert([
            "name" => "Manufacture Gus",
            "created_at" => now(),
            "updated_at" => now(),
        ]);

        DB::table('manufacture')->insert([
            "name" => "Manufacture Fabian",
            "created_at" => now(),
            "updated_at" => now(),
        ]);
    }
}
